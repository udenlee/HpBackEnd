<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>首页</title>
		<jsp:include page="head.jsp" />
		<!--main-->
		<div class="main warp cf">
			<%-- 首页位置1, 图片, 尺寸250x362 --%>
			<div class="main-left"><s:property value="results['L1'].content" escape="false"/></div>
			<div class="main-center">
				<div class="title bg1a"><span>医院动态</span><a taget="_blank" href="/news">更多</a></div>
				<div class="center-pic"><a href="javascript:void(0);"><img src="/assets/img/temp/news.jpg" alt="" width="420" height="110" /></a></div>
				<div class="center-tab">
					<!-- <ul class="tab">
						<li><a href="javascript:void(0);" class="this">最新动态</a></li>
					</ul> -->
					<div class="tab-content">
						<%-- <div class="tab-title"><a href="javascript:void(0);"><s:property value="results['article'].title" /></a></div>
						<p><s:property value="results['article'].summary" /><a class="xiangxi" href="javascript:void(0);">[详细]</a></p> --%>
						<ul>
						<s:iterator value="results['news']" var="a">
							<li><a href="/article?id=<s:property value="#a.id"/>"><s:property value="#a.title" /></a></li>
						</s:iterator>
						</ul>
					</div>
					<div class="tab-content hide">
						<div class="tab-title"><a href="javascript:void(0);"><s:property value="results['article'].title" /></a></div>
						<p><s:property value="results['article'].summary" /><a class="xiangxi" href="javascript:void(0);">[详细]</a></p>
						<ul>
						<s:iterator value="results['news']" var="a">
							<li><a href="javascript:void(0);"><s:property value="#a.title" /></a></li>
						</s:iterator>
						</ul>
					</div>
				</div>
			</div>
			<%-- 首页位置4 --%>
			<div class="main-right">
				<div class="title bg1a"><span>医院简介</span><a target="_blank" href="/overview">更多</a></div>
				<div class="profiles">
					<a href="/overview"><img src="<s:property value="results['L4'].image" />" alt="医院简介" width="286" height="150" /></a>
					<p><s:property value="results['L4'].summary" /><a class="xiangxi" target="_blank" href="<s:property value="results['L4'].link" />">[详细]</a></p>
				</div>
			</div>
		</div>

		<!--腰带广告-->
		<%-- 首页位置, 图片, 尺寸1000x90 --%>
		<div class="main-yd warp"><s:property value="results['L11'].content" escape="false"/></div>

		<!--专家介绍-->
		<div class="warp">
			<div class="modular">
				<div class="title bg1a"><span>专家介绍</span><a target="_blank" href="expert">更多</a></div>
				<div class="zj-info-box">
					<ul class="expert-box">
						<s:iterator value="results['expert']" var="e">
						<li>
							<div class="zj-pic"><a href="javascript:void(0);"><img src="<s:property value="#e.image" />" alt="专家<s:property value="#e.name" />" width="208" height="223" /></a></div>
							<div class="zj-info">
								<p><span class="name"><s:property value="#e.name" /></span><span class="posi"><s:property value="#e.position" /></span></p>
								<p class="info"><s:property value="#e.summary" /></p>
								<a class="zj-btn" target="_blank" href="<s:property value="#e.link" />">查看详细</a>
							</div>
						</li>
						</s:iterator>
					</ul>
				</div>
			</div>
		</div>

		<!--腰带广告-->
		<%-- 首页位置3, 图片, 尺寸1000x90 --%>
		<div class="main-yd warp"><s:property value="results['L10'].content" escape="false" /></div>

		<!--科室介绍-->
		<div class="warp">
			<div class="modular">
				<div class="title bg1a"><span>科室介绍</span><a href="/office">更多</a></div>
				<div class="keshi">
					<div class="pic-box">
						<ul class="keshi-box">
							<s:iterator value="results['office']" var="o">
							<li>
								<div class="keshi-title"><s:property value="#o.name" />简介</div>
								<div class="keshi-info cf">
									<div class="keshi-pic"><a target="_blank"  href="<s:property value="#o.link" />"><img width="220" height="120" src="<s:property value="#o.image" />" alt="<s:property value="#o.name" />图片" /></a></div>
									<p><s:property value="#o.summary" /><a target="_blank" href="<s:property value="#o.link" />">[更多]</a></p>
								</div>
							</li>
							</s:iterator>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="foot.jsp" />
		<script>
		(function () {
			//首页科室轮播
			$('.keshi-box').bxSlider({
				slideWidth: 578,
				minSlides: 2,
				maxSlides: 2,
				ticker: true,
				useCSS: false,
				tickerHover: true,
				speed: 80000,
				slideMargin: 20,
			})

			//首页专家轮播
			$('.expert-box').bxSlider({
				slideWidth: 578,
				minSlides: 2,
				maxSlides: 2,
				ticker: true,
				useCSS: false,
				tickerHover: true,
				speed: 30000,
				slideMargin: 10,
			})
			$('.nav li:eq(0) a').addClass('this')
		}())

		</script>
	</body>
</html>
