<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>后台登录</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/base.css">
    <style>
      body{font-family:"Microsoft Yahei";}
      .login{width:400px;height:320px;margin:110px auto;padding:40px 0;}
      .login-logo{padding-top:20px;height:56px;background:url(/assets/img/login.png) no-repeat bottom center;border-bottom:1px solid #357CB9;}
      .login ul{padding:0 56px;}
      .login ul li{margin-bottom:14px;position:relative;}
      .login ul li span{display:block;line-height:46px;position:absolute;left:0;top:0;z-index:2;font-size:14px;color:#808080;text-align:right;width:52px;padding-right:10px;}
      .login ul li img{vertical-align:middle;}
      .login input[type="text"],.login input[type="password"]{width:228px;background:#fff;height:46px;border:none;border-radius:5px;padding-left:60px;border:1px solid #ddd;}
      .login input[type="submit"]{display:block;border-radius:5px;background:#337ab7;color:#fff;width:288px;height:46px;font-size:16px;cursor: pointer;font-family:"Microsoft Yahei";}
      .login input[type="submit"]:hover{background:#2C699F;}
      .foot{text-align:center;color:#b1b1b1;line-height:30px;}
      .tips{text-align:center;color:#EA1919;line-height:30px;height:30px;}
    </style>
  </head>

  <body style="background:#f5f5f5;">
    <div class="login">
      <div class="login-logo"></div>
      <div class="tips"><span id="errorTip" style="display:none;"></span></div>
      <ul>
        <li><input id="username" type="text" /><span>账号:</span></li>
        <li><input id="password" type="password" /><span>密码:</span></li>
        <li><input id="captcha" style="width:120px;margin-right:10px;" type="text" /><span>验证码:</span><img id="captchaImg" width="96" height="36" src="/captcha.png" />
        </li>
        <li><input id="loginBtn" type="submit" value="登录" /></li>
      </ul>
    </div>
    <div class="foot">Copyright © 2016 昌图肛肠医院 版权所有</div>
    <script type="text/javascript" src="/assets/js/lib.js"></script>
    <script type="text/javascript">
    (function() {

      $('#captchaImg').on('click', function () {
        $(this).attr('src', '/captcha.png?t='+Math.random())
      })

      function errorTip (err) {
        $('#errorTip').text(err).show()
      }
      $('#loginBtn').on('click', function() {
        $('#errorTip').hide()
        var username = $('#username').val().trim()
        var password = $('#password').val().trim()
        var captcha = $('#captcha').val().trim()
        if (!username) {
          errorTip('用户名不能为空')
          return
        }
        if (!password) {
          errorTip('密码不能为空')
          return
        }
        if (!captcha) {
          errorTip('验证码不能为空')
          return
        }
        $.post('/admin/login', {
          username: username,
          password: password,
          vcode: captcha
        }, function(data) {
          if (data.err) {
            errorTip(data.err)
            $('#captchaImg').trigger('click')
          } else {
            location.href = '/admin/index'
          }
        }, 'json')
      })

      document.onkeydown = function(e) {
        var ev = document.all ? window.event : e;
        if(ev.keyCode == 13) {
          $('#loginBtn').trigger('click')
        }
      }
      $('#username').focus()
    }())
    </script>
  </body>
</html>