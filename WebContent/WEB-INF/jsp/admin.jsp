<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="zh">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>后台首页</title>
  <link rel="stylesheet" type="text/css" href="/assets/css/lib.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/font-awesome.css">
  <style type="text/css">
    .clearfix {clear:both;}
  </style>
</head>
<body>

<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<a class="admin-logo" href="/admin"><img src="/assets/img/admin-logo.png"  /></a>
		</div>
		<div class="admin-user-info">欢迎您，<span><s:property value="#session.admin.name"/></span><a href="/admin/logout">退出</a></div>
	</div>
</nav>
<section>
  <div class="container">
    <!-- 左侧菜单 -->
    <div class="col-xs-3 col-md-3" id="menu">
      <div class="sidebar-menu span2">
        <!-- 用户菜单 -->
        <a href="#userMeun" class="nav-header menu-first collapsed" data-toggle="collapse"><i
          class="fa fa-user"></i> 用户管理</a>
        <ul id="userMeun" class="nav nav-list collapse menu-second">
          <!-- <li><a id="itemUserAdd" class="menu-item" href="javascript:void(0);" data-url="user/add"><i class="icon-user"></i> 增加用户</a>
          </li>
          <li><a class="menu-item" href="javascript:void(0);" data-url="user/edit"><i class="icon-edit"></i> 修改用户</a>
          </li>
          <li><a class="menu-item" href="javascript:void(0);" data-url="user/delete"><i class="icon-trash"></i> 删除用户</a>
          </li> -->
          <li><a id="itemUserList" class="menu-item" href="javascript:void(0);" data-url="user/list"><i class="fa fa-users" aria-hidden="true"></i> 用户列表</a>
          </li>
        </ul>

        <!-- 文章菜单 -->
        <a href="#articleMenu" class="nav-header menu-first collapsed" data-toggle="collapse">
          <i class="glyphicon glyphicon-book"></i> 文章管理</a>
        <ul id="articleMenu" class="nav nav-list collapse menu-second">
          <li>
            <a id="itemArticleAdd" class="menu-item" href="javascript:void(0);" data-url="article/add"><i class="icon-pencil"></i>添加文章</a>
          </li>
          <li>
            <a id="itemArticleList" class="menu-item" href="javascript:void(0);" data-url="article/list"><i class="icon-list-alt"></i>文章列表</a></li>
        </ul>

        <!-- 位置菜单 -->
        <a href="#locationMenu" class="nav-header menu-first collapsed" data-toggle="collapse"><i
          class="glyphicon glyphicon-menu-hamburger"></i> 位置管理</a>
        <ul id="locationMenu" class="nav nav-list collapse menu-second">
          <!-- <li><a class="menu-item" href="javascript:void(0);" data-url="location/add"><i class="icon-pencil"></i>
            添加位置</a>
          </li> -->
          <li><a id="itemLocationList" class="menu-item" href="javascript:void(0);" data-url="location/list"><i
            class="icon-list-alt"></i>
            位置列表</a></li>
        </ul>

        <!-- 科室菜单 -->
        <a href="#officeMenu" class="nav-header menu-first collapsed" data-toggle="collapse"><i class="glyphicon glyphicon-menu-hamburger"></i> 科室管理</a>
        <ul id="officeMenu" class="nav nav-list collapse menu-second">
          <li>
            <a id="itemOfficeAdd" class="menu-item" href="javascript:void(0);" data-url="office/add"><i class="icon-pencil"></i>添加科室</a>
          </li>
          <li>
            <a id="itemOfficeList" class="menu-item" href="javascript:void(0);" data-url="office/list"><i class="icon-list-alt"></i>
            科室列表</a>
          </li>
        </ul>

        <!-- 专家菜单 -->
        <a href="#expertMenu" class="nav-header menu-first collapsed" data-toggle="collapse"><i class="glyphicon glyphicon-menu-hamburger"></i> 专家管理</a>
        <ul id="expertMenu" class="nav nav-list collapse menu-second">
          <li>
            <a id="itemExpertAdd" class="menu-item" href="javascript:void(0);" data-url="expert/add"><i class="icon-pencil"></i>添加专家</a>
          </li>
          <li>
            <a id="itemExpertList" class="menu-item" href="javascript:void(0);" data-url="expert/list"><i class="icon-list-alt"></i>
            专家列表</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="col-xs-9 col-md-9">
      <!-- 路径导航 -->
      <ol class="breadcrumb">
        <li id="title" class="active">{{title}}</li>
      </ol>
      <div class="panel panel-default">
        <div id="op-area" class="panel-body" style="min-height: 600px;">
        <div style="font-size: 50px;text-align: center;margin-top: 200px;font-weight: bold;color: gainsboro;">  欢迎来到后台管理系统</div>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="/assets/js/lib.js"></script>
<script src="/assets/src/route.js"></script>
<script src="/assets/src/data.js"></script>
<script src="/assets/js/layer.js"></script>
<script>
  (function () {
    var navVm = new Vue({
      el: '#title',
      data: {
        title : '首页'
      }
    })
    window.navVm = navVm
  }())
</script>
</body>
</html>