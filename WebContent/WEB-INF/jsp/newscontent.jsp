<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>新闻内页</title>
		<jsp:include page="head.jsp" />
		<!--list-->
		<div class="warp">
			<div class="list-content">
				<div class="list-l">
					<div class="title bg1a"><span>当前位置</span> > <span>新闻动态</span> > <span>文章列表</span></div>
					<div class="list-l-content">
						<div class="content-title"><s:property value="results['article'].title" /></div>
						<div class="content-tips"><span>发表时间：<em><s:date name="results['article'].add_time" format="yyyy-MM-dd" /></em></span><%-- <span>点击: <em><s:property value="results['article'].pv" /></em>次</span> --%></div>
						<div class="content-info"><s:property value="results['article'].content" escape="false" /></div>
						<div class="content-page"><a target="_blank" href="/nw/?id=<s:property value="results['prev'].id" />">上一篇：<span><s:property value="results['prev'].title" /></span></a></div>
						<div class="content-page"><a target="_blank" href="/nw/?id=<s:property value="results['next'].id" />">下一篇：<span><s:property value="results['next'].title" /></span></a></div>
					</div>
				</div>
				<s:action name="sidebar" executeResult="true"></s:action>
			</div>
		</div>
		<jsp:include page="foot.jsp" />
	</body>
</html>
