<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>新闻</title>
		<jsp:include page="head.jsp" />
		<!--list-->
		<div class="warp">
			<div class="list-content">
				<div class="list-l">
					<div class="title bg1a"><span>当前位置</span> > <span>新闻动态</span></div>
					<div class="list-l-content">
						<ul>
							<s:iterator value="results['list']" var="a">
							<li>
								<div class="list-title"><a href="/article?id=<s:property value="#a.id"/>"><s:property value="#a.title" /></a></div>
								<p><s:property value="#a.summary" /><a class="more" href="/article?id=<s:property value="#a.id"/>">[更多]</a></p>
							</li>
							</s:iterator>
						</ul>
					</div>
					<div class="list-page">
						<input type="hidden" id="limit" value="<s:property value="results.limit" />" />
						<input type="hidden" id="page" value="<s:property value="results.page" />" />
						<input type="hidden" id="count" value="<s:property value="results.count" />" />
						<div class="page" id="news-page"></div>
					</div>
				</div>
				<s:action name="sidebar" executeResult="true"></s:action>
			</div>
		</div>
		<jsp:include page="foot.jsp" />
	</body>
	<script>
	$('.nav li:eq(2) a').addClass('this')
	</script>
</html>
