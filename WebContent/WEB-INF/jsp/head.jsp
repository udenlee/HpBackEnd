<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
	<link rel="stylesheet" type="text/css" href="/assets/css/base.css" />
	<link rel="stylesheet" type="text/css" href="/assets/css/home.css" />
	<link rel="stylesheet" type="text/css" href="/assets/css/listcontent.css" />
	<link rel="stylesheet" type="text/css" href="/assets/css/simplePagination.css" />
</head>
<body>
	<!--top-->
	<div class="top">
		<div class="warp">
			<div class="top-left">您好，欢迎来到昌图肛肠医院</div>
			<div class="top-right">
				<ul>
					<li><a href="/">网站首页</a>|</li>
					<li><a href="/contacts">联系电话</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!--logo-->
	<div class="warp cf">
		<div class="logo-box">
			<a href="javascript:void(0);"><s:property value="results['L2'].content" escape="false"/></a>
		</div>
		<div class="logo-info"><s:property value="results['L3'].content" escape="false"/></div>
	</div>
	<!--导航-->
	<div class="nav bg1a">
		<div class="warp">
			<ul>
				<li><a href="/">首页</a></li>
				<li><a href="/overview">医院介绍</a></li>
				<li><a href="/news">新闻动态</a></li>
				<li><a href="/office">科室介绍</a></li>
				<li><a href="/expert">专家介绍</a></li>
				<li><a href="/contacts">联系我们</a></li>
			</ul>
		</div>
	</div>
	<!--banner-->
	<div class="banner">
		<img src="/assets/img/temp/banner.jpg" alt="" width="1000" height="360" />
	</div>