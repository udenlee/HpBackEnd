<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>专家介绍</title>
		<jsp:include page="head.jsp" />
		<!--list-->
		<div class="warp">
			<div class="list-content">
				<div class="list-l">
					<div class="title bg1a"><span>当前位置</span> > <span>专家介绍</span></div>
					<div class="list-l-content">
						<ul>
							<s:iterator value="results['list']" var="e">
							<li>
								<div class="expert cf">
									<div class="expert-pic"><a href="expertcontent.html"><img src="<s:property value="#e.image" />" alt="<s:property value="#e.name" />图片" width="208" height="223"></a></div>
									<div class="expert-info">
										<div class="info">
											<p><span class="name"><s:property value="#e.name" /></span><span class="posi"><s:property value="#e.position" /></span></p>
											<p class="shanc">擅长：<span><s:property value="#e.major" /></span></p>
											<p class="exp-content"><s:property value="#e.summary" /></p>
											<a class="exper-btn" href="/expertIndex?id=<s:property value="#e.id" />">查看详细</a>
										</div>
									</div>
								</div>
							</li>
							</s:iterator>
						</ul>
					</div>
					<div class="list-page">
						<input type="hidden" id="limit" value="<s:property value="results.limit" />" />
						<input type="hidden" id="page" value="<s:property value="results.page" />" />
						<input type="hidden" id="count" value="<s:property value="results.count" />" />
						<div class="page" id="news-page"></div>
					</div>
				</div>
				<s:action name="sidebar" executeResult="true"></s:action>
			</div>
		</div>
		<jsp:include page="foot.jsp" />
	</body>
	<script>
	$('.nav li:eq(4) a').addClass('this')
	</script>
</html>
