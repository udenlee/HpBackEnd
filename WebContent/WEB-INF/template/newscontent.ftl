<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>新闻内页</title>
		<link rel="stylesheet" type="text/css" href="css/base.css" />
		<link rel="stylesheet" type="text/css" href="css/home.css" />
		<link rel="stylesheet" type="text/css" href="css/listcontent.css" />
	</head>
	<body>
		<#include "./head.ftl">
		<!--list-->
		<div class="warp">
			<div class="list-content">
				<div class="list-l">
					<div class="title bg1a"><span>当前位置</span> > <span>新闻动态</span> > <span>文章列表</span></div>
					<div class="list-l-content">
						<div class="content-title">无痛胃肠镜检查的优点有哪些？</div>
						<div class="content-tips"><span>发表时间：<em>2016-03-17</em></span><span>点击: <em>20</em>次</span></div>
						<div class="content-info">
							<p>无痛胃镜和肠镜是采用一种新的无痛技术，只需在静脉中注入少量药物，病人即可在安详的睡眠中完成检查和治疗</p>
							<p>1.无痛苦：患者在检查、治疗过程中无任何不舒服，对精神紧张的患者、对胃肠镜检查恐惧的患者，无痛胃镜、无痛肠镜是您的理想选择。 </p>
							<p>2.创伤小：在无痛性电子胃镜下，对消化道出血、息肉、溃疡狭窄还可以进行多项微创 治疗，让患者免于手术开刀之苦。</p>
							<p>3.时间短：排除检查前的预备时间，从检查开始，几分钟内即可完成。</p>
							<p>4.更精确：电子胃肠镜拥有目前其他检查手段无法代替的优势，尤其是一些微小病变甚至粘膜层的病变，均可明确诊断。并且具有放大功能，更进一步增加了诊断的准确性。医生能够用肉眼直接观察到消化道的内部情况，能够发现诸如溃疡、息肉、肿瘤、憩室炎症等病变，还能看清黏膜的充血、水肿以及色泽改变等细微变化，并且可以在直视下活检做病理检查和镜下切除息肉、止血、抓取异物等。</p>
							<p>传统胃肠镜检查，时间长、痛苦多、往往恶心、呕吐、腹痛给胃肠镜检查带来困难。</p>
						</div>
						<div class="content-page"><a href="javascript:void(0);">上一篇：<span>女性易便秘的原因</span></a></div>
						<div class="content-page"><a href="javascript:void(0);">下一篇：<span>有哪些人群适合做大肠水疗？</span></a></div>
					</div>
				</div>
				<div class="list-r">
					<div class="r-list">
						<div class="title bg1a"><span>关于我们</span><a href="javascript:void(0);">更多</a></div>
						<div class="r-content">
							<img src="img/jianjie.jpg" alt="" width="258" height="100" />
							<p>医院是指以向人提供医疗护理服务为主要目的医疗机构。其服务对象不仅包括患者和伤员，也包括处于特定生理状态的健康人（如孕妇、产妇、新生儿）以及完全健康的人。</p>
						</div>
					</div>
					<div class="r-list">
						<div class="title bg1a"><span>新农合住院实行分段报销</span></div>
						<div class="r-content">
							<dl>
								<dt>0～500元，报销50%；</dt>
								<dt>500～2000元，报销80%；</dt>
								<dt>2000元以上，报销60%；</dt>
								<dt>低保户、五保户在正常报销比例的基础上，再增加15%。</dt>
								<dd>例如：一位患复杂性肛瘘的普通手术患者住院总费用为3000元，报销费用为2050元，个人付费为950元。</dd>
							</dl>
						</div>
					</div>
					<div class="r-list">
						<div class="title bg1a"><span>联系我们</span></div>
						<div class="r-content">
							<ul>
								<li><span>联系电话：</span>74492555</li>
								<li><span>联系地址：</span>昌图火车站东50米</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<#include "./foot.ftl">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/hospital.js"></script>
	</body>
</html>
