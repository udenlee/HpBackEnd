<!--top-->
<div class="top">
	<div class="warp">
		<div class="top-left">您好，欢迎来到昌图肛肠医院</div>
		<div class="top-right">
			<ul>
				<li><a href="/">网站首页</a>|</li>
				<li><a href="/contacts">联系电话</a></li>
			</ul>
		</div>
	</div>
</div>
<!--logo-->
<div class="warp cf">
	<div class="logo-box">
		<a href="javascript:void(0);"><img src="/assets/img/logo.png" alt="" width="330" height="80" /></a>
	</div>
	<div class="logo-info"><img src="/assets/img/logo-info.jpg" alt="" width="600" height="80" /></div>
</div>
<!--导航-->
<div class="nav bg1a">
	<div class="warp">
		<ul>
			<li><a href="/">首页</a></li>
			<li><a href="/overviews">医院介绍</a></li>
			<li><a href="/news">新闻动态</a></li>
			<li><a href="/technic">科室介绍</a></li>
			<li><a href="/experts">专家介绍</a></li>
			<li><a href="/contacts">联系我们</a></li>
		</ul>
	</div>
</div>
<!--banner-->
<div class="banner">
	<img src="/assets/img/temp/banner.jpg" alt="" width="1000" height="360" />
</div>