<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>首页</title>
		<link rel="stylesheet" type="text/css" href="/assets/css/base.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/home.css" />
	</head>
	<body>
		<#include "./head.ftl">

		<!--main-->
		<div class="main warp cf">
			<div class="main-left"><img src="/assets/img/bg-ct.jpg" alt="" width="250" height="362" /></div>
			<div class="main-center">
				<div class="title bg1a"><span>医院动态</span><a href="newslist.html">更多</a></div>
				<div class="center-pic"><a href="javascript:void(0);"><img src="/assets/img/temp/news.jpg" alt="" width="420" height="110" /></a></div>
				<div class="center-tab">
					<ul class="tab">
						<li><a href="javascript:void(0);" class="this">最新动态</a></li>
						<li><a href="javascript:void(0);">经典案例</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-title"><a href="javascript:void(0);">怎样预防大肠息肉癌变？</a></div>
						<p>结肠癌，直肠癌，大多是由肠息肉发展而来的，也就是说，大肠息肉是癌前病变。大肠里面有息肉，一定要提高警惕。临床上大肠癌主要来源于腺瘤样肠息肉，息肉越大，生长时间越长，越容易癌变。是埋伏在体内的“定时炸弹”。很多人发现大便带血或便血，都首先误认为痔疮，其实很多是肠息肉引起的便血。生活中很多人惧怕肠镜检查，这也就导致了很多患息肉的人最终因发现较晚，演变成大肠癌。这样的悲剧不胜枚举，所以一定要高度警惕大肠息肉的癌变。对出现便血、粘液便、大便次数增多或肛门下坠感等，一定要及时做结肠镜检查，发现大肠息肉要及时切除，避免肠息肉发生癌变。<a class="xiangxi" href="javascript:void(0);">[详细]</a></p>
						<ul >
							<li><a href="javascript:void(0);">常见便血的原因？</a></li>
							<li><a href="javascript:void(0);">哪些人需要接受肠镜检查？</a></li>
							<li><a href="javascript:void(0);">无痛胃肠镜检查的优点有哪些？</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="main-right">
				<div class="title bg1a"><span>医院简介</span><a href="auout.html">更多</a></div>
				<div class="profiles">
					<a href="auout.html"><img src="/assets/img/jianjie.jpg" alt="" width="286" height="150" /></a>
					<p>昌图肛肠医院是一所以治疗肛肠病为主的专科医院，也是昌图县内唯一的肛肠专科医院，是全县肛肠疾病的预防、诊断、治疗中心。是新型农村合作医疗、城镇医疗保险定点报销医院。医院技术力量雄厚，人才密集，专业设备齐全，仪器设备先进，整体医疗水平高，在全县具有良好声誉。医院坚持以中西医结合，微创手术，术中、术后长效止痛为治疗特色。坚持以病人为中心，全心全意为患者服务的理念，在全省率先开展了绿色就医通道。医院特聘请沈阳市肛肠医院肛肠外科主任宫学范来我院长期全天出诊，亲自为肛肠病患者进行手术，擅长治疗复杂性痔、肛周脓肿、肛瘘、直肠脱垂、溃疡性结肠炎、结肠息肉、大肠肿瘤及便秘，尤其对各种高位复杂瘘等肛肠疑难病的诊断和治疗具有较深造诣。“微创无痛”手术治疗各类肛肠疾病数万人，具有较高影响力。主任医师王效忠曾任沈阳市肿瘤研究所所长，沈阳市肛肠医院外科主任，沈阳医学院教授、在多种肿瘤、各种肛肠疾病的诊断和治疗方面具有丰富临床经验。普外科主任医师李森源，曾任铁岭市中心医院普外科主任，擅长治疗甲状腺、乳腺、肝胆、无张力疝修补术等，尤其擅长胃癌、结肠癌、直肠癌手术，技术精湛。我院还与省级医院建立了协作关系，腔镜中心聘请了中国医科大学四院李卫东教授每周日来我院出诊、开展无痛胃镜、无痛肠镜的检查与治疗，在全县率先开展了胃肠息肉、腺瘤、结肠早癌等无痛氩气刀电切微创手术。昌图肛肠医院拥有多台大型高端精密仪器，如进口奥林巴斯胃镜、奥林巴斯肠镜、飞利浦彩超、氩气刀、全结肠水疗机、高频痔疮治疗机等。昌图肛肠医院随着医院的快速发展加强了与国内著名肛肠诊疗中心联合诊治的脚步，邀请肛肠领域知名的顶级教授、专家现场指导。昌图肛肠医院在强化区域品牌的同时，继续挖掘并优化医院的品牌资源，在政府和社会各界的支持下，为不久的将来把医院打造成辽北区域肛肠病治疗中心和国内知名的肛肠专科医院，为广大患者提供更优质的服务而努力。<a class="xiangxi" href="javascript:void(0);">[详细]</a></p>
				</div>
			</div>
		</div>

		<!--腰带广告-->
		<div class="main-yd warp"><img src="/assets/img/temp/yaodai.jpg" alt="" width="1000" height="90" /></div>

		<!--专家介绍-->
		<div class="warp">
			<div class="modular">
				<div class="title bg1a"><span>专家介绍</span><a href="expert.html">更多</a></div>
				<div class="zj-info-box">
					<ul class="expert-box">
						<li>
							<div class="zj-pic"><a href="javascript:void(0);"><img src="/assets/img/temp/zjpic01.jpg" alt="" width="208" height="223" /></a></div>
							<div class="zj-info">
								<p><span class="name">王喜阁</span><span class="posi">院长</span></p>
								<p class="info">辽宁省中西医结合学会肛肠专业委员会委员，从事肛肠外科、胃肠镜工作近二十年。采用微创手术治疗混合痔、复杂性肛瘘、环状痔PPH术、TST手术。开展痔疮、肛瘘术后的长效止痛新技术。对溃疡性结肠炎、慢性结肠炎性验疗</p>
								<a class="zj-btn" href="javascript:void(0);">查看详细</a>
							</div>
						</li>
						<li>
							<div class="zj-pic"><a href="javascript:void(0);"><img src="/assets/img/temp/zjpic02.jpg" alt="" width="208" height="223" /></a></div>
							<div class="zj-info">
								<p><span class="name">李森源</span><span class="posi">主任医师 外科主任</span></p>
								<p class="info">毕业于中国医科大学,原铁岭市中心医院普外科专家。辽宁省医学会外科学会副主任委员，从事外科工作四十五年，经验丰富。擅长甲状腺、肝胆、乳腺、无张力疝修补术等普外科常见手术，尤其对胃癌、结肠癌、直肠癌等消化道肿瘤的手术治疗达到国内先进水平。</p>
								<a class="zj-btn" href="javascript:void(0);">查看详细</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!--腰带广告-->
		<div class="main-yd warp"><img src="/assets/img/temp/yaodai.jpg" alt="" width="1000" height="90" /></div>

		<!--科室介绍-->
		<div class="warp">
			<div class="modular">
				<div class="title bg1a"><span>科室介绍</span><a href="keshilist.html">更多</a></div>
				<div class="keshi">
					<div class="pic-box">
						<ul class="keshi-box">
							<li>
								<div class="keshi-title">疝外科简介</div>
								<div class="keshi-info cf">
									<div class="keshi-pic"><a href="javascript:void(0);"><img width="220" height="120" src="" alt="" /></a></div>
									<p>昌图肛肠医院疝气科开展疝气无张力修补术取得非常满意的临床效果，尤其对于高龄、慢性气管炎、肺气肿等合并疝的效果都非常良好。传统的疝气修补手术要把缺损周边组织强行缝合起来，术后切口疼痛明显，复发率也比较高。采用无张力疝气修补手术，手术时间短，用网片修补，不增加周围组织张力，术后无明显的疼痛牵扯感。手术当日即可下床活动，住院时间短，术后三天即可出院。临床证明无张力疝修补术是一种最佳的疝气治疗方法。<a href="javascript:void(0);">[更多]</a></p>
								</div>
							</li>
							<li>
								<div class="keshi-title">肛肠外科简介</div>
								<div class="keshi-info cf">
									<div class="keshi-pic"><a href="javascript:void(0);"><img width="220" height="120" src="" alt="" /></a></div>
									<p>肛肠外科是我院的核心科室，在沈阳市肛肠医院专家宫学范主任的带领下突破多项难题，成功治愈了各种复杂疑难肛肠疾病，特别是采用微创手术，使患者在术中术后基本无痛，全面开展了PPH/TST等肛肠手术，深受本地区和周边地区患者的好评。<a href="javascript:void(0);">[更多]</a></p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<#include "./foot.ftl">
	</body>
</html>
