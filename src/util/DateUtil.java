package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DateUtil {
	
	public final static long MINUTE = 60 * 1000;// 1分钟
	public final static long HOUR = 60 * MINUTE;// 1小时
	public final static long DAY = 24 * HOUR;// 1天
	public final static long MONTH = 31 * DAY;// 月
	public final static long YEAR = 12 * MONTH;// 年
	
	/**
	 * 如果遇到时差问题，请在Java配置中，输入default vm argument ： -Duser.timezone=GMT+08
	 */

	/** 获得int型日期数值 */
	/**
	 * 获得当前年月日
	 */
	public static int getYear() {
		return Calendar.getInstance().get(Calendar.YEAR);
	}
	public static int getMonth() {
		return Calendar.getInstance().get(Calendar.MONTH) + 1;
	}
	public static int getDay() {
		return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	}
	public static int getDayOfWeek(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_WEEK);
	}
	
	/**
	 * 获取指定日期那一周的第一天(第一天是周一)
	 * @param date
	 * @return
	 * @author 王涛
	 * 2016年1月6日
	 */
	public static Date getFirstDayOfWeek(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH)-1);
		}
		c.set(Calendar.DAY_OF_WEEK, 2);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}
	
	/**
	 * 获取指定日期那一周的第一天(第一天是周一)
	 * @return
	 * @author 王涛
	 * 2016年1月6日
	 */
	public static Date getFirstDayOfWeek(){
		Calendar c = Calendar.getInstance();
		if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH)-1);
		}
		c.set(Calendar.DAY_OF_WEEK, 2);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}

	/** 获得String型日期值 */
	public String getStrMonth() {
		if (getMonth() < 10)
			return "0" + getMonth();

		return getMonth() + "";
	}
	public String getStrDay() {
		if (getDay() < 10)
			return "0" + getDay();

		return getDay() + "";
	}

	/**
	 * 按时间格式获得字符型当前日期
	 * @param formatStr 格式化格式
	 * @return
	 */
	public static String getFormatDate(String formatStr) {
		if ("".equals(formatStr)) {
			formatStr = "yyyy-MM-dd";
		}
		SimpleDateFormat bartDateFormat = new SimpleDateFormat(formatStr);
		return bartDateFormat.format(new Date()).toString();
	}

	/**
	 * 传入字符串，返回日期
	 * @param strDate 	日期字符串
	 * @param fmt 		日期格式
	 * @return
	 */
	public static Date strToDate(String strDate, String fmt) {
		if (AllUtil.isNull(strDate)) {
			return null;
		} else {
			if (AllUtil.isNull(fmt))
				fmt = "yyyy-MM-dd";
			SimpleDateFormat format = new SimpleDateFormat(fmt);

			Date date = null;
			try {
				date = format.parse(strDate);
			} catch (ParseException e) {}

			return date;
		}
	}

	/**
	 * 传入日期和天数，返回增加天数后的日期
	 * @param sDate
	 * @param days
	 * @return
	 */
	public static String getNewDateByAddDays(String sDate, int days) {
		Date date = new Date(strToDate(sDate, "").getTime() + days * DAY);
		return dateToStr(date, "yyyy-MM-dd");
	}

	/**
	 * 传入日期和天数，返回增加天数后的日期
	 * @param sDate
	 * @param days
	 * @return
	 */
	public static Date getNewDateByAddDay(Object date, int days) {
		long mills = 0l;
		if (date instanceof String) {
			mills = strToDate((String) date, "").getTime() + days * DAY;
		} else {
			mills = ((Date) date).getTime() + days * DAY;
		}
		return new Date(mills);
	}

	/**
	 * 传入日期和月数，返回增加月数后的日期
	 * @param sDate
	 * @param month		可正可负
	 * @return
	 */
	public static String getNewDateByAddMonth(String sDate, int month) {

		int testY = Integer.parseInt(sDate.split("-")[0]);
		int testM = Integer.parseInt(sDate.split("-")[1]) - 1;
		int testD = Integer.parseInt(sDate.split("-")[2]);

		GregorianCalendar cal = new GregorianCalendar(testY, testM, testD);
		cal.add(GregorianCalendar.MONTH, month);

		return dateToStr(strToDate(cal.get(1) + "-" + (cal.get(2) + 1) + "-" + cal.get(5), ""), "yyyy-MM-dd");
	}

	/**
	 * 传入日期和月数，返回增加月数后的日期
	     * @param sDate
	     * @param month         可正可负
	     * @return
	     */
	public static  Date getDateByAddMonth(String sDate, int month) {

		int testY = Integer.parseInt(sDate.split("-")[0]);
		int testM = Integer.parseInt(sDate.split("-")[1]) - 1;
		int testD = Integer.parseInt(sDate.split("-")[2]);

		GregorianCalendar cal = new GregorianCalendar(testY, testM, testD);
		cal.add(GregorianCalendar.MONTH, month);

		return strToDate(cal.get(1) + "-" + (cal.get(2) + 1) + "-" + cal.get(5), "");
	}
	
	/**
	 * 判断给定日期是否是当月最后一天
	 * @param day
	 * @return
	 * @author 王涛
	 * 2015年10月19日
	 */
	public static boolean isLastDayOfMonth(Date day){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(day);
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * 传入日期，返回字符串
	 * @param date 日期
	 * @param fmt 	字符串格式
	 * @return
	 */
	public static String dateToStr(Date date, String fmt) {
		if (null == date) {
			return null;
		} else {
			SimpleDateFormat format = new SimpleDateFormat(fmt);
			return format.format(date);
		}
	}

	/**
	 * 传入生日，获得年龄
	 * @param bir
	 * @return
	 */
	public static long birthToAge(Date birth) {
		return birth == null ? 0 : new Date().getTime() - birth.getTime() / YEAR;
	}

	/**
	 * 两个日期相差多少天
	 * @param beginDate		开始日期
	 * @param endDate		截止日期
	 * @return
	 */
	public static Integer dateToDays(String beginDate, String endDate) {
		return (int) (strToDate(endDate, "").getTime() / DAY - strToDate(beginDate, "").getTime() / DAY);
	}

	/**
	 * 两个日期相差多少个月零多少天
	 * @param beginDate		开始日期，格式必须是yyyy-mm-dd
	 * @param endDate		截止日期，格式必须是yyyy-mm-dd
	 * @return
	 */
	public static String getMDBy2Date(String beginDate, String endDate) {

		int testY = Integer.parseInt(beginDate.split("-")[0]);
		int testM = Integer.parseInt(beginDate.split("-")[1]) - 1;
		int testD = Integer.parseInt(beginDate.split("-")[2]);

		GregorianCalendar cal = new GregorianCalendar(testY, testM, testD);
		cal.add(GregorianCalendar.MONTH, 1);

		//获得过去了多少月
		int month = 0;
		String test = cal.get(1) + "-" + (cal.get(2) + 1) + "-" + cal.get(5);
		while (dateToDays(endDate, test) > -2) {
			cal.add(GregorianCalendar.MONTH, 1);
			test = cal.get(1) + "-" + (cal.get(2) + 1) + "-" + cal.get(5);
			month++;
		}

		//获得过去了多少天
		cal.add(GregorianCalendar.MONTH, -1);
		test = cal.get(1) + "-" + (cal.get(2) + 1) + "-" + cal.get(5);
		long days = dateToDays(endDate, test) + 1;

		return month + ";" + days;
	}

	/**
	 * 传入时间和要减少的分钟数，返回时间
	 * @return
	 * @author 王国庆
	 */
	public static String getSubMinite(Date date, int time) {
		Calendar now = Calendar.getInstance();
		now.setTime(date);
		now.set(Calendar.MINUTE, now.get(Calendar.MINUTE) + time);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		return sdf.format(now.getTime());
	}

	public static Date getAddDay(Date date, int day) {
		Calendar now = Calendar.getInstance();
		now.setTime(date);
		now.add(Calendar.DATE, day);

		return now.getTime();
	}

	/**
	 * 返回文字描述的日期
	 * @author 张妍
	 * @param date
	 * @return
	 */
	public static String getTimeFormatText(Date date) {
		if (date == null) {
			return null;
		}
		long diff = new Date().getTime() - date.getTime();
		long r = 0;
		if (diff > YEAR) {
			r = (diff / YEAR);
			return r + "年前";
		} else if (diff > MONTH) {
			r = (diff / MONTH);
			return r + "个月前";
		} else if (diff > DAY) {
			r = (diff / DAY);
			return r + "天前";
		} else if (diff > HOUR) {
			r = (diff / HOUR);
			return r + "个小时前";
		} else if (diff > MINUTE) {
			r = (diff / MINUTE);
			return r + "分钟前";
		} else {
			return "刚刚";
		}

	}
	/**
	 * 将时间格式转换成年月日时分秒
	 * @author 张妍
	 * 2014-04-02
	 * @param date
	 * @return str
	 */
	public static String getDateToString(Date date) {
		String str = "";
		SimpleDateFormat fymd = new SimpleDateFormat("yyyy年MM月dd日    HH时mm分");
		str = fymd.format(date);

		return str;
	}
	
	/**
	 * 两个日期相差多少天，小时，分，秒
	 * @param beginDate  开始日期，
	 * @param endDate		截止日期，
	 * @param format			日期格式
	 * @author 张妍
	 * @edit 王涛
	 * 2014-05-07
	 * @throws ParseException 
	 */
	public static String dateDiff(String startTime, String endTime, String format) {
		SimpleDateFormat sd = new SimpleDateFormat(format);
		//获得两个时间的毫秒时间差异
		long diff = 0;
		try {
			diff = sd.parse(endTime).getTime() - sd.parse(startTime).getTime();
		} catch (Exception e) {}
		//若开始时间大于结束时间，返回0
		if (diff < 0) {
			return "0";
		}
		long day = diff / DAY;//计算差多少天
		long hour = diff % DAY / HOUR;//计算差多少小时
		long min = diff % DAY % HOUR / MINUTE;//计算差多少分钟
		long sec = diff % DAY % HOUR % MINUTE / 1000;//计算差多少秒//输出结果

		return " " + day + "天" + hour + "小时" + min + "分" + sec + "秒  ";
	}
	/**
	 * 两个日期相差多少天，小时，分，秒
	 * @param beginDate  开始日期，
	 * @param endDate		截止日期，
	 * @param format			日期格式
	 * @author 张妍
	 * 2014-09-02
	 * @throws ParseException 
	 */
	public long getDistanceTime(long diff, int timetype) {
		long result = 0;
		//若开始时间大于结束时间，返回0
		if (diff < 0) {
			return 0;
		}
		if (timetype == 0) {
			result = diff / DAY;
		} else if (timetype == 1) {
			result = diff / HOUR;
		} else if (timetype == 2) {
			result = diff / MINUTE;
		} else if (timetype == 3) {
			result = diff / 1000;
		}
		return result;
	}
	
	/**
	 * 两个日期相差毫秒
	 * @param beginDate  开始日期，
	 * @param endDate		截止日期，
	 * @param format			日期格式
	 * @author 王涛
	 * 2014年6月13日
	 */
	public static Long dateDiff2Long(String startTime, String endTime, String format) {
		SimpleDateFormat sd = new SimpleDateFormat(format);
		try {
			return sd.parse(endTime).getTime() - sd.parse(startTime).getTime();
		} catch (Exception e) {
			return 0l;
		}
	}
	
	/**
	 * 两个日期之间相差多少天
	 * @author 王涛
	 * 2015年3月31日
	 */
	public static int daysDiff(String startTime, String endTime, String format) {
		return (int)Math.floor(( strToDate(startTime, format).getTime() - strToDate(endTime, format).getTime() ) / DAY);
	}
	
	/**
	 * 两个日期之间相差多少天
	 * @author 王涛
	 * 2015年3月31日
	 */
	public static int daysDiff(Date startTime, Date endTime) {
		return (int)Math.floor(( startTime.getTime() - endTime.getTime() ) / DAY);
	}

	/**
	 * 传入一个日期，判断是否是今天
	 * @param date (字符串和日期都可以,字符串请按yyyy-MM-dd传入)
	 * @author 王涛
	 * 2015年1月8日
	 */
	public static boolean isCurrentDay(Object date) {
		if (date instanceof String) {
			return strToDate((String) date, "yyyy-MM-dd").getTime() == GetZeroTime().getTime();
		} else if (date instanceof Date) {
			return GetZeroTime().getTime() == GetZeroTime((Date) date).getTime();
		} else {
			return false;
		}
	}
	
	/**
	 * 获得指定日期的最后时刻, 即 23:59:59.999
	 * @param date
	 * @return
	 * @author 王涛
	 * 2015年12月30日
	 */
	public static Date GetEndTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		if (date != null) { 
			calendar.setTime(date);
		}
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}
	
	/**
	 * 获得当天日期的最后时刻, 即 23:59:59.999
	 * @return
	 * @author 王涛
	 * 2015年12月30日
	 */
	public static Date GetEndTime() {
		return GetEndTime(null);
	}

	/**
	 * 获得指定日期零点时间, 即 00:00:00.000
	 * @param date 要计算的日期
	 * @author 王涛
	 * 2015年1月8日
	 */
	public static Date GetZeroTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		if (date != null) { 
			calendar.setTime(date);
		}
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	/**
	 * 获得当天零点时间, 即 00:00:00.000
	 * @author 王涛
	 * 2015年1月8日
	 */
	public static Date GetZeroTime() {
		return GetZeroTime(null);
	}
	
	/**
	 * day减1
	 * @param date 要计算的日期
	 * @author WangGQ 2015-6-19 10:48:17
	 */
	public static Date GetYesterday(Date date) {
		Calendar calendar = Calendar.getInstance();
		if (date != null) {
			calendar.setTime(date);
		}
		calendar.add(Calendar.DATE, -1);
		return calendar.getTime();
	}
	
	/**
	 * 取指定时间的地板时间
	 * 例 ： 17:59 取 17:00
	 * @author 王涛
	 * 2015年4月23日
	 */
	public static Date GetFloorTime(Date date) {
		int hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
		return new Date(GetZeroTime(date).getTime() + hours * HOUR);
	}
	
	/**
	 * 取当前时间的地板时间
	 * 例 ： 17:59 取 17:00
	 * @author 王涛
	 * 2015年4月23日
	 */
	public static Date GetFloorTime() {
		return GetFloorTime(null);
	}
	
	/**
	 * 取指定时间的天花板时间
	 * 例 ： 17:59 取 18:00
	 * @author 王涛
	 * 2015年4月23日
	 */
	public static Date GetCeilTime(Date date) {
		int hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + 1;
		return new Date(GetZeroTime(date).getTime() + hours * HOUR);
	}
	
	/**
	 * 取当前时间的天花板时间
	 * 例 ： 17:59 取 18:00
	 * @author 王涛
	 * 2015年4月23日
	 */
	public static Date GetCeilTime() {
		return GetCeilTime(null);
	}
	
	/**
	 * 获取该月的天数
	 * @author WangGQ
	 * 2015-6-3 17:01:52
	 */
	public static int GetDayOfMonth(int year, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * 判断传入的两个日期是否是同一天
	 * @param firstDate
	 * @param secoundDate
	 * @return
	 * 
	 * @author WangGQ 2015-7-9 09:55:35
	 */
	public static boolean isSameDay(Date firstDate, Date secoundDate){
		if (DateUtil.dateToStr(firstDate, "yyyy-MM-dd").equals(dateToStr(secoundDate, "yyyy-MM-dd"))) {
			return true;
		}
		
		return false;
	}
	
	public static Date getOrganizationTime(Date joinDate){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		
		Calendar calendar_join = Calendar.getInstance();
		calendar_join.setTime(joinDate);
		
		if (calendar.get(Calendar.DAY_OF_MONTH) == 1) {
			calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH)-1);
		}
		
		if (calendar.get(Calendar.MONTH) == calendar_join.get(Calendar.MONTH)) {
			return joinDate;
		}else {
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			
			return calendar.getTime();
		}
	}
}
