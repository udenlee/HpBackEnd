package util;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.serializer.SerializerFeature;


/**
 * JSON 共通应用类
 * @author 	王子莅
 * @editor	韩晗		直接输出Json字符串
 * @editor	韩晗	2015-01-28
 * 			增加将Json字符串转成List<Map>或Map对象的方法
 */
public class JSONUtil {
	
	
	/**
	 * 创建一个临时mmGrid配置对象
	 * @param page 页数
	 * @param limit 每页显示记录数
	 * @param totalCount 总记录数
	 * @param items 集合
	 * @return 临时mmGrid配置对象
	 */
	private static Object createMmGridSetting(int page, int limit, int totalCount, Object items) {
		HashMap<String, Object> mmGridSetting = new HashMap<String, Object>();
		mmGridSetting.put("page", page);
		mmGridSetting.put("limit", limit);
		mmGridSetting.put("totalCount", totalCount);
		mmGridSetting.put("items", items);
		return mmGridSetting;
	}
	
	
	/**
	 * 对response输出JSON格式的mmGrid配置
	 * @param page 页数
	 * @param limit 每页显示记录数
	 * @param totalCount 总记录数
	 * @param items 集合
	 */
	public static void responseMmGridSettingInJSON(int page, int limit, int totalCount, Object items) {
		responseJSON(createMmGridSetting(page, limit, totalCount, items));
	}
	
	
	/**
	 * 直接在action相应response对象中输出JSON字符串
	 * 输出后需在对应action返回"none", action设置可参照struts-showcase.xml中名为tree的action, result处值为空
	 * @param obj 传入任意类型对象
	 */
	public static void responseJSON(Object obj, SerializerFeature[] features) {
		HttpServletResponse res = ServletActionContext.getResponse();

		res.setContentType("text/html;charset=utf-8");
		res.setCharacterEncoding("utf-8");
		
		try {
			PrintWriter out = res.getWriter();
			
			out.write(JSON.toJSONString(obj, features));
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 默认对response输出JSON方法, 所应用的序列化配置如下:
	 * 		枚举类型输出为字符串
	 * 		日期类型转为字符串
	 * @param obj
	 */
	public static void responseJSON(Object obj) {
		SerializerFeature[] features = {
			SerializerFeature.WriteEnumUsingToString,
			SerializerFeature.WriteDateUseDateFormat
		};
		responseJSON(obj, features);
	}
	
	
	/**
	 * 把字符串直接输出到页面
	 * @param str 字符串
	 */
	public static void responseString(String str) {
		HttpServletResponse res = ServletActionContext.getResponse();

		res.setContentType("text/html;charset=utf-8");
		res.setCharacterEncoding("utf-8");
		
		try {
			PrintWriter out = res.getWriter();
			
			out.write(str);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	@SuppressWarnings("rawtypes")
	public static List<Map> parseList(String jsonStr) {
		return JSONArray.parseArray(jsonStr, Map.class);
	}
	
	public static List<?> parseList(String jsonStr,Class<?> clazz) {
		return JSONArray.parseArray(jsonStr, clazz);
	}
	
	@SuppressWarnings("rawtypes")
	public static Map parseMap(String jsonStr) {
		return JSONArray.parseObject(jsonStr, Map.class);
	}
	
}