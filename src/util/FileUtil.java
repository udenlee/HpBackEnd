package util;


import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author 韩晗		2014-01-12
 */
public class FileUtil {
	
	
	/**
     * 创建目录
     * @param DirName
     */
    public boolean createDir(String dirPath) {
    	if(!dirPath.endsWith(File.separator)) {
    		dirPath = dirPath + File.separator;
    	}

    	File dir = new File(dirPath);
    	if(dir.mkdirs()) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    
    /**
     * 文件或目录是否存在
     * @param path
     */
    public boolean fileExists(String path) {
    	File file = new File(path);
    	return file.exists();
    }
    
    /**
     * 提取文件扩展名
     * @param path
     */
    public String fileExt(String path) {
    	String ext = "";
    	File file = new File(path);
    	
    	if(file.isFile()) {
    		String filename = file.getName();
    		ext = filename.substring(filename.indexOf('.')+1).toLowerCase();
    	}
    	
    	return ext;
    }
    
    
	/**
	 * 删除目录下的所有文件或文件夹
	 * @param path	路径
	 */
	public static void delDirectory(String path) {
		
		File dir=new File(path);
		if(dir.exists()) {
			if(dir.isFile()) {
				dir.delete();
			}
			else if(dir.isDirectory()) {
				if(dir.exists()) {
					File[] tmp=dir.listFiles();
					for(int i=0; i<tmp.length; ++i) {
						if(tmp[i].isDirectory()) {
							delDirectory(tmp[i].getPath());
						}
						else {
							tmp[i].delete();
						}
					}
					dir.delete();
			    }
			}
		}
	}


    /**   
     * @param	filePath 文件路径
     * @return  获得File的全部内容
     */   
    public String readTextFile(String filePath, String charset) {
    	
        BufferedReader 	br	= null;
        StringBuffer	sb 	= new StringBuffer();
        
        try {
        	if("".equals(charset)) {
        		charset = "utf-8";
        	}
            br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), charset));
            String temp = null;
            while((temp=br.readLine())!=null){
                sb.append(temp + "\r\n");
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
        	if(br!=null) {
        		try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
        	}
        }
        
        return sb.toString();
    }

    
    /**
     * 创建一个文本类文件，并将代码写入
     * 文本类如：txt、htm、jsp等
     * @param filePath
     * @param textCode
     * @return
     */
	public boolean writeFile(String filePathAndName, String fileContent, String charset) {
		try {
			if("".equals(charset)) {
        		charset = "utf-8";
        	}
			
			File f = new File(filePathAndName);
			if(!f.exists()) {
				f.createNewFile();
			}
   
			OutputStreamWriter 	write = new OutputStreamWriter(new FileOutputStream(f), charset);
			BufferedWriter 		writer= new BufferedWriter(write);
			writer.write(fileContent);
			writer.close();
			return true;
		}
		catch(IOException e) {
    		e.getStackTrace();
    		return false;
    	}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		} 
	}

	
	// 遍历文件夹下的文件夹与文件的绝对地址
	public static List<String> listAllDirAndFileAbsolutePath(String strPath) {
		return getFileResult(strPath).get("listAbsAll");
	}

	// 遍历文件夹下的文件夹与文件的名字
	public static List<String> listAllDirAndFileName(String strPath) {
		return getFileResult(strPath).get("listAllName");
	}

	// 遍历文件夹下的文件夹的绝对地址
	public static List<String> listAllDirAbsolutePath(String strPath) {
		return getFileResult(strPath).get("listAbsDir");
	}

	// 遍历文件夹下的文件夹的名字
	public static List<String> listAllDirName(String strPath) {
		return getFileResult(strPath).get("listDirName");
	}

	// 遍历文件夹下的文件的绝对地址
	public static List<String> listAllFileAbsolutePath(String strPath) {
		return getFileResult(strPath).get("listAbsFile");
	}

	// 遍历文件夹下的文件的名字
	public static List<String> listAllFileName(String strPath) {
		return getFileResult(strPath).get("listFileName");
	}

	private static Map<String, List<String>> getFileResult(String strPath) {
		Map<String, List<String>> mapList = new HashMap<String, List<String>>();
		List<String> listAbsDir = new ArrayList<String>(); 		// 文件夹绝对路径集合
		List<String> listAbsFile = new ArrayList<String>(); 	// 文件名绝对路径集合
		List<String> listAbsAll = new ArrayList<String>(); 		// 所有文件及文件夹集合
		List<String> listDirName = new ArrayList<String>(); 	// 文件夹集合
		List<String> listFileName = new ArrayList<String>(); 	// 文件名集合
		List<String> listAllName = new ArrayList<String>(); 	// 所有文件及文件夹名称集合

		File dir = new File(strPath); 					// 传路径
		File[] files = dir.listFiles(); 				// 返回目录下所有文件
		for (File file : files) {
			if (file.isDirectory()) {					// 测试数组中的文件名是否是个目录
				listDirName.add(file.getName());
				listAbsDir.add(file.getAbsolutePath());
			} else {
				listFileName.add(file.getName());
				listAbsFile.add(file.getAbsolutePath());
			}
			listAbsAll.add(file.getAbsolutePath());
			listAllName.add(file.getName());
		}
		mapList.put("listDirName", listDirName);
		mapList.put("listAbsDir", listAbsDir);
		mapList.put("listFileName", listFileName);
		mapList.put("listAbsFile", listAbsFile);
		mapList.put("listAbsAll", listAbsAll);
		mapList.put("listAllName", listAllName);
		return mapList;
	}
}
