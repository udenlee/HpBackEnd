package util;


import java.util.List;

import core.comm.Propertie;


public class HqlUtil {
	
	
	/**
	 * 获取HQL语句片段
	 * @param properties	属性列表
	 * @return HQL语句片段
	 */
	public static String getHqlSetters(List<Propertie> prots) {
		StringBuffer hql = new StringBuffer();
		
		int index = 1;
		String and = "";
		for(int i=0; i<prots.size(); i++) {
			Propertie prot = prots.get(i);
			if("set".equals(prot.getType())) {
				if("".equals(prot.getOperator())) {
					hql.append(and).append(" ").append(prot.getValue());
				}
				else {
					hql.append(and).append(" ").append(prot.getName()).append("=:s").append(index++);
				}

				if(and.equals("")) {
					and = ", ";
				}
			}
		}
		
		return hql.toString();
	}
	
	
	/**
	 * 获取HQL语句片段
	 * @param properties	属性列表
	 * @return HQL语句片段
	 */
	public static String getHqlFinders(List<Propertie> prots) {
		StringBuffer hql = new StringBuffer();
		
		int index = 1;
		String and = "";
		for(int i=0; i<prots.size(); i++) {
			Propertie prot = prots.get(i);
			if("where".equals(prot.getType())) {
				if("".equals(prot.getOperator())){
					hql.append(and).append(prot.getValue()).append(" ");
				}
				else if("in".equals(prot.getOperator())) {
					hql.append(and).append(" ").append(prot.getName()).
								append(" in (:f").append(index++).append(") ");
				}
				else if("not in".equals(prot.getOperator())) {
					hql.append(and).append(" ").append(prot.getName()).
								append(" not in (:f").append(index++).append(") ");
				}
				else if("like".equals(prot.getOperator())) {
					hql.append(and).append(" ").append(prot.getName()).
								append(" like :f").append(index++).append(" ");
				}
				else {
					hql.append(and).append(" ").append(prot.getName()).
								append(prot.getOperator()).append(":f").append(index++).append(" ");
				}
				
				if(and.equals("")) {
					and = "and ";
				}
			}
		}
		
		return hql.toString();
	}
	
}
