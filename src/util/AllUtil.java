package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import core.comm.Propertie;


/**
 * 用户工具类(user模块外请慎用)
 * @author 王涛
 */
public class AllUtil {
	private static Log logger = LogFactory.getLog(AllUtil.class);
	public static final String USER_DEFAULT_LOGO = "/upload/head/noHead.jpg"; 
	public static final String MALL_DEFAULT_PIC = "/resource/mall/images/logo_bg.jpg";
	public static final String OG_DEFAULT_LOGO = "/upload/logoCircle/sc.jpg";
	public static final String USERNAME_REG = "^[0-9a-zA-Z]{6,16}$";
	public static final String PASSWORD_REG = "^[0-9a-zA-Z]{6,20}$";
	public static final String MAIL_REG = "^[a-zA-Z0-9_]{6,12}+@+[a-zA-Z0-9.]+(\\.[a-zA-Z]+){1,3}$";
	public static final String QQ_REG = "^\\d{6,11}$";
	public static final String YY_REG = "^\\d{6,20}$";
	
	/**
	 * 正则匹配字符串，如果符合正则规范返回true，否则返回false 
	 * @param regEx 		正则表达式 例：^[a-z]$、[^a-z]、\\w{5,11}
	 * @param checkStr   被校验的字符串
	 * @return true || false
	 * @author 王涛
	 */
	public static boolean regCheck(String regEx, String checkStr) {
		if (checkStr == null) return false;
		try {
			return Pattern.compile(regEx).matcher(checkStr).find() ? true : false;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 计算字符串长度
	 * @param str 要计算的字符串
	 * @return 字符串长度
	 * @author 王涛
	 */
	public static Integer realLength(String str) {
		String len = "";
		int j = 0;
		char[] c = str.toCharArray();
		for (int i = 0; i < c.length; i++) {
			len = Integer.toBinaryString(c[i]);
			if (len.length() > 8) {
				j += 2;
			} else {
				j++;
			}
		}
		return j;
	}

	/**
	 * 判断字符串是否在<=指定长度 (真实长度，一个汉字=2个英文字符)
	 * str的长度>len 返回true
	 * 其他返回false
	 * @param str 待检测的字符串
	 * @param len 最大长度
	 * @author 王涛
	 * 2014年7月17日
	 */
	public static Boolean campLength(String str, Integer len) {
		return realLength(str) < parseInt(len);
	}

	/**
	 * 判断字符串是否在指定长度内[minLen, maxLen](真实长度，一个汉字=2个英文字符)
	 * @param 
	 * @author 王涛
	 * 2014年7月17日
	 */
	public static Boolean campLength(String str, Integer minLen, Integer maxLen) {
		return realLength(str) <= parseInt(maxLen) && realLength(str) >= parseInt(minLen);
	}
	
	/**
	 * 自动拼where条件
	 * @param sql
	 * @param props
	 * @return
	 * @author 王涛
	 * 2015年12月12日
	 */
	public static String addWhere(String sql, List<Propertie> props) {
		if (props == null || props.size() == 0) {
			return sql;
		}
		sql += " WHERE " + HqlUtil.getHqlFinders(props);
		return sql;
	}
	
	/**
	 * 自动拼where条件
	 * @param sql
	 * @param props
	 * @return
	 * @author 王涛
	 * 2015年12月12日
	 */
	public static String addWhere(String sql, List<Propertie> props, String sort) {
		sql = addWhere(sql, props);
		if (!isNull(sort)) {
			sql += " ORDER BY " + sort;
		}
		return sql;
	}

	/**
	 * 拼查询条件
	 * @param query Dao传入的query对象
	 * @param prots  hql查询条件
	 * @param page   分页参数page(可选)
	 * @param limit    分页参数每页显示条数(可选)
	 * @author 王涛
	 * 2014年8月12日
	 */
	public static Query modifyQuery(Query query, List<Propertie> props, Integer page, Integer limit) {

		query = modifyQuery(query, props);

		// 添加分页参数
		if (page != null && limit != null && page > 0) {
			query.setFirstResult((page - 1) * limit);
			query.setMaxResults(limit);
		}
		return query;
	}

	/**
	 * 拼查询条件
	 * @param query Dao传入的query对象
	 * @param prots  hql查询条件
	 * @author 王涛
	 * 2014年8月12日
	 */
	public static Query modifyQuery(Query query, List<Propertie> props) {
		int indexSeter = 1;
		int indexFinder = 1;
		for (Propertie prot : props) {
			if (!"".equals(prot.getOperator())) {
				if ("set".equals(prot.getType())) {
					query.setParameter("s" + indexSeter, prot.getValue());
					indexSeter++;
				} else if ("where".equals(prot.getType())) {
					if (prot.getOperator().indexOf("in") != -1) {
						List<Integer> intList = new ArrayList<Integer>();
						String[] tempArray = String.valueOf(prot.getValue()).split(",");
						for (String var : tempArray) {
							intList.add(Integer.valueOf(var));
						}
						query.setParameterList("f" + indexFinder, intList);
						indexFinder++;
					} else {
						query.setParameter("f" + indexFinder, prot.getValue());
						indexFinder++;
					}
				}
			}
		}
		return query;
	}

	/**
	 * 拼查询条件
	 * @param query Dao传入的query对象
	 * @param prots  hql查询条件
	 * @param page   分页参数page(可选)
	 * @param limit    分页参数每页显示条数(可选)
	 * @author 王涛
	 * 2014年8月12日
	 */
	public static SQLQuery modifySQLQuery(SQLQuery query, List<Propertie> props) {

		int indexSeter = 1;
		int indexFinder = 1;
		for (Propertie prot : props) {
			if (!"".equals(prot.getOperator())) {
				if ("set".equals(prot.getType())) {
					query.setParameter("s" + indexSeter, prot.getValue());
					indexSeter++;
				} else if ("where".equals(prot.getType())) {
					if (prot.getOperator().indexOf("in") != -1) {
						List<Integer> intList = new ArrayList<Integer>();
						String[] tempArray = String.valueOf(prot.getValue()).split(",");
						for (String var : tempArray) {
							intList.add(Integer.valueOf(var));
						}
						query.setParameterList("f" + indexFinder, intList);
						indexFinder++;
					} else {
						query.setParameter("f" + indexFinder, prot.getValue());
						indexFinder++;
					}
				}
			}
		}
		return query;
	}

	/**
	 * 拼查询条件
	 * @param query Dao传入的query对象
	 * @param prots  hql查询条件
	 * @param page   分页参数page(可选)
	 * @param limit    分页参数每页显示条数(可选)
	 * @author 王涛
	 * 2014年8月12日
	 */
	public static SQLQuery modifySQLQuery(SQLQuery query, List<Propertie> props, Integer page, Integer limit) {

		query = modifySQLQuery(query, props);

		// 添加分页参数
		if (page != null && limit != null && page > 0) {
			query.setFirstResult((page - 1) * limit);
			query.setMaxResults(limit);
		}
		return query;
	}

	/**
	 * 将对象转成数字，转换错误返回0
	 * @param obj 待转换对象
	 * @return int 转换后的数字，转换错误返回0
	 * @author 王涛
	 * 2014年10月9日
	 * @throws InterruptedException 
	 */
	public static int parseInt(Object obj) {
		try {
			if (obj == null || obj.equals("")) {
				return 0;
			} else {
				return Integer.parseInt(obj.toString());
			}
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * 将对象转成float，转换错误返回0
	 * @param obj 待转换对象
	 * @return float 转换后的数字，转换错误返回0
	 * @author 顾越
	 * 2014年12月5日
	 */
	public static float parseFloat(Object obj) {
		try {
			if (obj == null) {
				return 0;
			} else {
				return ((Float) obj).floatValue();
			}
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * 判断正负
	 * @param num 待计算数字
	 * @return 0：0 ， 1 ： 负数 ，2 ： 正数
	 * @author 王涛
	 * 2014年10月9日
	 */
	public static int judgePN(int num) {
		return num == 0 ? 0 : num > 0 ? 2 : 1;
	}

	/**
	   * 计算表名
	   * @param userId 用户id
	   * @param length 表名长度
	   * @author 王涛
	   * 2014年11月5日
	   */
	public static String calTableName(Long userId, int length) {

		int radix = (int) Math.pow(10.0, length - 1);
		String tableName = userId.intValue() % radix + "";
		for (int i = 0; i < length - 1; i++) {
			if (tableName.length() < length - 1) {
				tableName = "0" + tableName;
			} else {
				break;
			}
		}
		return tableName;
	}

	/**
	 * Hibernate 逐出方法
	 * @param session Hibernate session
	 * @param object 要逐出的对象
	 * @return obj 对象
	 * @author 王涛
	 * 2014年10月14日
	 */
	public static Object evict(Session session, Object obj) {
		if (obj instanceof List<?>) {
			List<?> list = (List<?>) obj;
			for (Object tmp : list) {
				_evict(session, tmp);
			}
		} else {
			_evict(session, obj);
		}
		return obj;
	}

	/**
	 * 去除全部空格
	 * @param replacedStr 需要替换的字符串
	 * @return 输入null和空字符串返回null
	 * @author 王涛
	 * 2014年12月11日
	 */
	public static String trimAll(String replacedStr) {
		if (replacedStr != null && replacedStr.length() > 0) {
			return replacedStr.replaceAll(" ", "").replaceAll("　", "");
		} else {
			return null;
		}
	}

	/**
	 * Hibernate 逐出list方法
	 * @param session Hibernate session
	 * @param <Object> objList 要逐出的对象列表
	 * @return objList 对象列表
	 * @author 王涛
	 * 2014年10月14日
	 */
	private static Object _evict(Session session, Object obj) {
		if (obj == null) {
			return null;
		}
		session.evict(obj);
		return obj;
	}

	/**
	 * 获得个人头像切图
	 * @param originPath 图片路径
	 * @param size 尺寸(如：96x96)
	 * @author 王涛
	 * 2014年12月24日
	 */
	public static String thumbnailLogo(String originPath, String size) {
		return PictureCut.CropImg(originPath, size, size, Picture.TYPE_GEOMETRIC, Picture.FLAG_LOGO).toString();
	}

	/**
	 * 生成6位随机数
	 * @author 王涛
	 * 2014年10月18日
	 */
	public static String random() {

		Random rand = new Random();
		String seed = "23456789qwertyuipasdfghjkzxcvbnm";
		String code = "";
		for (int i = 0; i < 6; i++) {
			code += seed.charAt(rand.nextInt(32));
		}
		return code;
	}
	
	/**
	 * 随机生成指定长度的数字字符串，默认4位
	 * 
	 * @param length
	 * @return
	 * @author 王涛
	 * 2015年9月18日
	 */
	public static String randomNumberCode(Integer length) {
		if (length == null) length = 4;
		String seed = "1234567890";
		String code = "";
		Random rand = new Random();
		for (int i = 0; i < length; i++) {
			code += seed.charAt(rand.nextInt(10));
		}
		return code;
	}
	
	public static String randomNumberCode() {
		return randomNumberCode(4);
	}
	
	/**
	 * 生成[1,max]范围内随机整数
	 * @param max 最大数
	 * @author 王涛
	 * 2014年10月18日
	 */
	public static int randomInt(Integer max) {
		return randomInt(1, max);
	}

	/**
	 * 生成[min,max]范围内随机整数
	 * @param min 最小数
	 * @param max 最大数
	 * @author 王涛
	 * 2014年10月18日
	 */
	public static int randomInt(Integer min, Integer max) {
		Random random = new Random();
		return random.nextInt(max) % (max - min + 1) + min;
	}

	/**
	 * 判断是否为null
	 * @param checkedStr 
	 * @author 王涛
	 * 2015年1月3日
	 */
	public static Boolean isNull(String checkedStr) {
		return checkedStr == null || checkedStr.isEmpty();
	}

	/**
	 * 判断对象是否所有属性全为空
	 * 
	 * 此方法利用反射执行该类的所有get方法，判断get方法的返回值是否为空。
	 * 如果po的“get”方法不是以get开头，那么此方法将无效
	 * 
	 * @author 王涛
	 * 2015年1月15日
	 */
	public static Boolean isPropertiesEmpty(Object entity) {

		// 获得该类的反射
		Class<?> clazz = entity.getClass();
		Method[] methods = clazz.getDeclaredMethods();
		for (int i = 0; i < methods.length; i++) {
			if (methods[i].getName().startsWith("get")) {

				String var = methods[i].getName();

				var = (var.substring(3, var.length()).toLowerCase());
				try {
					Object object = methods[i].invoke(entity);

					if (object != null && !isNull(object.toString().trim())) {
						return false;
					}
				} catch (Exception e) {
				}

			}
		}
		return true;
	}

	/**
	 * null -> ""
	 * @author 王涛
	 * 2015年1月16日
	 */
	public static String handleNull(String checkedStr) {
		return checkedStr == null ? "" : checkedStr;
	}

	/**
	 * 格式化数字为两位数字符串
	 * 输入 1 返回 01
	 * 输入 12 返回12
	 * @author 王涛
	 * 2015年1月28日
	 */
	public static String format2Double(Object object) {
		return object.toString().length() == 2 ? object.toString() : "0" + object.toString();
	}

	/**
	 * 格式化数字成任意位长度数字字符串
	 * 输入 1 返回 01
	 * 输入 12 返回12
	 * @param object 待格式化数字
	 * @param length 长度
	 * @author 王涛
	 * 2015年1月28日
	 */
	public static String format2Any(Object object, int length) {
		String str = object.toString();
		while (str.length() < length) {
			str = "0" + str;
		}
		return str;
	}

	/**
	 * 去掉右空格，再剪切字符串
	 * @author 韩晗
	 * @param str			源字符串
	 * @param n				要裁切的字符串长度
	 */
	public static String cutStr(String str, int n) {

		if (str == null || "null".equals(str.toLowerCase())) {
			return "";
		}

		while (!str.equals("") && str.charAt(str.length() - 1) == ' ') {
			str = str.substring(0, str.length() - 1);
		}

		if (str.length() > n) {
			return str.substring(0, n) + "…";
		}

		return str;
	}
	
	/**
	 * 查询指定元素的前一个元素
	 * @author 王涛
	 * 2015年4月2日
	 */
	public static int preEl(int curEl, int arr[]) {
		for(int i = 0; i < arr.length; i ++) {
			if(curEl == arr[i]) return i == 0 ? 0 : arr[i-1];
		}
		return 0;
	}
	
	/**
	 * 查询指定元素的前一个元素
	 * @author 王涛
	 * 2015年4月2日
	 */
	public static int nextEl(int curEl, int arr[]) {
		int length = arr.length;
		for(int i = 0; i < length; i ++) {
			if(curEl == arr[i]) return i == length ? 0 : arr[i+1];
		}
		return 0;
	}

	/**
	 * 获取当前时间戳的后 n 位，不足按 0 补齐
	 * 
	 * @author 王涛
	 * 2015年5月6日
	 */
	public static String timeStamp(int length) {
		return  format2Any(new Date().getTime() %  ((long)Math.pow(10.0, 16 - (double)length)), length);
	}
	
	/**
	 * 0->a,1->b,2->c,4->d
	 * @param charcode
	 * @return
	 * @author 王涛
	 * 2015年6月24日
	 */
	public static String getChar(int charcode) {
		return new Character((char) (charcode + 65)).toString();
	}
	
	/**
	 * a->0,b->1,c->1,d->4
	 * @param charStr
	 * @return
	 * @author 王涛
	 * 2015年6月24日
	 */
	public static int getCharCode(String charStr) {
		return charStr.charAt(0) - 97;
	}
	
	/**
	 * 格式化double类型数字
	 * @param v  原始数字 如：3.414
	 * @param scale 小数点后几位 如：2
	 * @return
	 * @author 王涛
	 * 2015年8月31日
	 */
	public static Double DoubleFormat(double v, int scale) {
		return new BigDecimal(v).setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	/**
	 * 判断是否是数字
	 * 
	 * @param str
	 * @return
	 * @author 王涛
	 * 2015年9月10日
	 */
	public static Boolean isNumber(String checkStr) {
		return regCheck("^\\d+$", checkStr);
	}
	
	/**
	 * 骆驼命名转下划线命名
	 * 如：isNumberLine 转成 is_number_line
	 * 
	 * @param str
	 * @return
	 * @author 王涛
	 * 2015年9月16日
	 */
	public static String camelCase2Underscore(String str) {
		String REPLACE = "_";
		Matcher m = Pattern.compile("[A-Z]").matcher(str);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			m.appendReplacement(sb, REPLACE + m.toMatchResult().group().toLowerCase());
		}
		m.appendTail(sb);
		return sb.toString();
	}
	
	/**
	 * 根据ip获取分表号
	 * @param ip
	 * @return 分表号
	 * @author 顾越
	 */
	public static String getTableNum(String ip){
		String tableNum = null;
		String[] ips = ip.split("\\.");
		tableNum = ips[3];
		return tableNum;
	}
	
	/**
	 * 
	 * @param field
	 * @param newValue
	 * @throws Exception
	 * @author 王涛 2015年10月21日
	 */
	public static void setFinalStatic(Field field, Object newValue) throws Exception {
		try {
			field.setAccessible(true);

			Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
			modifiersField.set(field, field.getModifiers() & ~Modifier.FINAL);

			field.set(null, newValue);
		} catch (Exception e) {}
	}
	
	/**
	 * 判断某属性是否是基本类型 例如: Integer:true/CommonUser:false
	 * @param field
	 * @return
	 * @author 王涛
	 * 2015年12月12日
	 */
	public static boolean isPrimitive(Field field) {
		String type = field.getType().toString();
		return type.indexOf("yisi") == -1;
	}
	
	/**
	 * 过滤特殊字符
	 * @param dirtyStr
	 * @return
	 * @author 王涛
	 * 2015年12月12日
	 */
	public static String cleanStr(String dirtyStr) {
		if (dirtyStr == null) return null;
		dirtyStr = dirtyStr.trim();
		return dirtyStr.replace("\"", "")
					.replace("\'", "")
					.replace(";'", "")
					.replace("'", "")
					.replace(">", "")
					.replace("$", "")
					.replace("%", "")
					.replace("<", "");
	}
	
	public static String upload (File file, String dirPath, String name) {
		try {
			// 先创建文件夹
			File dir = new File(dirPath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			// 获取文件名&保存文件
			String filename = name.substring(0, name.lastIndexOf(".")) +
							name.substring(name.lastIndexOf(".")).toLowerCase();
			FileInputStream fis = new FileInputStream(file);
			FileOutputStream fos = new FileOutputStream(dir + File.separator + filename);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = fis.read(buffer)) > 0) {
				fos.write(buffer, 0, length);
			}
			fis.close();
			fos.close();
			return null;
		} catch (Exception e) {
			logger.error("上传失败", e);
			return "上传失败";
		}
	}
	
	/**
	 * 异或(两者一方为空, 一方不为空返回true, 其余返回false)
	 * @param arg1
	 * @param arg2
	 * @return
	 * @author 王涛
	 * 2016年1月12日
	 */
	public static boolean XOR (Object arg1, Object arg2) {
		return (arg1 == null && arg2 != null) || (arg1 != null && arg2 == null);
	}
	
	/**
	 * 同或(两者同时为空或者同时不为空返回true，其余返回false)
	 * @param arg1
	 * @param arg2
	 * @return
	 * @author 王涛
	 * 2016年1月28日
	 */
	public static boolean XNOR (Object arg1, Object arg2) {
		return (arg1 == null && arg2 == null) || (arg1 != null && arg2 != null);
	}
	
	/**
	 * 与 
	 * @param arg1
	 * @param arg2
	 * @return
	 * @author 王涛
	 * 2016年1月12日
	 */
	public static boolean AND(Object arg1, Object arg2) {
		return arg1 != null && arg2 != null;
	}
}
