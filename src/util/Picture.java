package util;

public class Picture {
	
	public static final int TYPE_SAME_WIDTH = 1;		//等宽
	public static final int TYPE_SAME_HEIGHT = 2;		//等高
	public static final int TYPE_GEOMETRIC = 3;			//等比
	public static final int TYPE_SOURCE_REGION = 4;		//指定大小
	
	public static final boolean TYPE_CUT = true;		//切割
	public static final boolean TYPE_NO_CUT = false;	//不切割
	
	public static final int FLAG_YISINET = 1;			//yisinet默认图
	public static final int FLAG_CRY = 2;				//相册默认图
	public static final int FLAG_LOGO = 3;				//个人头像默认图
	public static final int FLAG_OG = 4;				//圈子头像默认图
	
	public static final boolean VALUE_MAP = true;		//返回map类型
	public static final boolean VALUE_URL = false;		//返回url
	
	public static final boolean IS_FILLER = true;		//补白
	public static final boolean IS_NO_FILLER = false;	//不补白
	
}
