package util;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

@SuppressWarnings("serial")
public class StrUtil implements java.io.Serializable{

	/**
	 * 按当前时间获得一个新的数字字符串
	 * @param prefix	生成的字符串前缀
	 */
	public static String getNewName(String prefix){
	    String newname = DateUtil.getFormatDate("yyyyMMddHHmmssSSS");
	    return prefix + newname;
	}

	
	/**
	 * 清除字符串前后以及中间的所有空格
	 * @param str
	 */
	public static String trimAll(String str){
	    while (str.indexOf(" ") != -1) {
	    	str = str.substring(0, str.indexOf(" ")) + str.substring(str.indexOf(" ") + 1);
	    }
	    return str;
	}
	
	/**
	 * 将textarea文本变成html格式
	 */
	public static String textarea2Html(String str) {
		str += "";
		str = str.replaceAll(" ", "&nbsp;");
		str = str.replaceAll("<", "&lt;");
		str = str.replaceAll(">", "&gt;");
		str = str.replaceAll("\r\n", "<br/>");
		str = str.replaceAll("\r", "<br/>");
		str = str.replaceAll("\n", "<br/>");
		return str;
	}
	
	public static String unescape(String str) {
		str += "";
		str = str.replaceAll("&nbsp;", " ");
		str = str.replaceAll("&lt;", "<");
		str = str.replaceAll("&gt;", ">");
		return str;
	}
	
	/**
	 * 将textarea文本变成html格式
	 */
	public static String textarea2AllHtml(String str) {
		str += "";
		str = str.replaceAll("&", "&amp;");
		str = str.replaceAll("\"", "&quot;");
		str = str.replaceAll("￠", "&cent;");
		str = str.replaceAll("£", "&pound;");
		str = str.replaceAll("¥", "&yen;");
		str = str.replaceAll("€", "&euro;");
		str = str.replaceAll("§", "&sect;");
		str = str.replaceAll("©", "&copy;");
		str = str.replaceAll("®", "&reg;");
		str = str.replaceAll("™", "&trade;");
		str = str.replaceAll("×", "&times;");
		str = str.replaceAll("÷", "&divide;");
		str = str.replaceAll("<", "&lt;");
		str = str.replaceAll(">", "&gt;");
		return str;
	}
	
	/**
	 * 将textarea文本换行改为空格
	 */
	public static String textarea2SimpleText(String str) {
		str += "";
		str = str.replaceAll("\r\n", " ");
		str = str.replaceAll("\r", " ");
		str = str.replaceAll("\n", " ");
		str = str.replaceAll("\t", " ");
		return str;
	}
	
	/**
	 * 判断内容是否含有非法字符
	 *
	 * @author 王国庆
	 */
	public static  boolean validateBody(String str){
		boolean isSuc = true;
		
		Matcher mj = Pattern.compile("<script(.*?)/script>").matcher(str.toLowerCase());
		Matcher mf = Pattern.compile("<iframe(.*?)/iframe>").matcher(str.toLowerCase());
		Matcher ms = Pattern.compile("<style(.*?)/style>").matcher(str.toLowerCase());
		Matcher mj1 = Pattern.compile("<script(.*?)/>").matcher(str.toLowerCase());
		Matcher mf1 = Pattern.compile("<iframe(.*?)/>").matcher(str.toLowerCase());
		Matcher ms1 = Pattern.compile("<style(.*?)/>").matcher(str.toLowerCase());
		
		if (mj.find()||mf.find()||ms.find()||mj1.find()||mf1.find()||ms1.find()) {
			isSuc = false;
		}
		
		return isSuc;
	}
	
	/**
	 * 过滤危险单词
	 * 
	 * @author 王国庆
	 */
	public static String filterWord(String str ,String word){
		
		int le = 0;
		if (word.split(",").length > 0) {
			for (int i = 0; i < word.split(",").length; i++) {
				if (str.contains(word.split(",")[i])) {
					le++;
					str = str.replaceAll(word.split(",")[i], "*****");
				}
			}
		}
		
		str = str.replaceAll("onabort", "ｏnabort");
		str = str.replaceAll("onblur", "ｏnblur");
		str = str.replaceAll("onchange", "ｏnchange");
		str = str.replaceAll("onclick", "ｏnclick");
		str = str.replaceAll("ondblclick", "ｏndblclick");
		str = str.replaceAll("onerror", "ｏnerror");
		str = str.replaceAll("onfocus", "ｏnfocus");
		str = str.replaceAll("onkeydown", "ｏnkeydown");
		str = str.replaceAll("onkeypress", "ｏnkeypress");
		str = str.replaceAll("onkeyup", "ｏnkeyup");
		str = str.replaceAll("onload", "ｏnload");
		str = str.replaceAll("onmousedown", "ｏnmousedown");
		str = str.replaceAll("onmousemove", "ｏnmousemove");
		str = str.replaceAll("onmouseout", "ｏnmouseout");
		str = str.replaceAll("onmouseover", "ｏnmouseover");
		str = str.replaceAll("onmouseup", "ｏnmouseup");
		str = str.replaceAll("onreset", "ｏnreset");
		str = str.replaceAll("onresize", "ｏnresize");
		str = str.replaceAll("onselect", "ｏnselect");
		str = str.replaceAll("onsubmit", "ｏnsubmit");
		str = str.replaceAll("onunload", "ｏnunload");
		str = str.replaceAll("altKey", "ａltKey");
		str = str.replaceAll("button", "ｂutton");
		str = str.replaceAll("clientX", "ｃlientX");
		str = str.replaceAll("clientY", "ｃlientY");
		str = str.replaceAll("ctrlKey", "ｃtrlKey");
		str = str.replaceAll("metaKey", "ｍetaKey");
		str = str.replaceAll("relatedTarget", "ｒelatedTarget");
		str = str.replaceAll("screenX", "ｓcreenX");
		str = str.replaceAll("screenY", "ｓcreenY");
		str = str.replaceAll("shiftKey", "ｓhiftKey");
		str = str.replaceAll("href", "ｈref");
		
		
		if (le > 10) {
			str = "";
		}
		
		return str;
	}


	/**
	 * 去掉右空格，再剪切字符串
	 * @param str			源字符串
	 * @param n				要裁切的字符串长度
	 */
	public static String cutStr(String str, int n) {
		
	    if (str==null || "null".equals(str.toLowerCase())) {
	    	return "";
	    }
	    
	    while(!str.equals("") && str.charAt(str.length()-1)==' ') {
	    	str = str.substring(0, str.length()-1);
	    }
	    
	    if (str.length() > n) {
	    	return str.substring(0, n) + "…";
	    }
	
	    return str;
	}

  
	/**
	 * 传入一个double值，返回小数点后两位的值，不足两位用零占位
	 */
	public static String doubleFormat(double d) {
		java.text.NumberFormat formatter = java.text.NumberFormat.getNumberInstance();
		formatter.setMinimumFractionDigits(2);
		formatter.setMaximumFractionDigits(2);
	  
		return formatter.format(d);
	}
	
	
	/**
	 * 传入一个double值，返回小数点后n位的值，不足n位用零占位
	 */
	public static String doubleFormat(double d, int n) {
		java.text.NumberFormat formatter = java.text.NumberFormat.getNumberInstance();
		formatter.setMinimumFractionDigits(n);
		formatter.setMaximumFractionDigits(n);
	  
		return formatter.format(d);
	}
	
	
	/**
	 * 将[id1][id2][id3]转成id1,id2,id3
	 * @param ids:[id1][id2][id3]
	 */
	public static String commaIds(String ids) {
		ids = ids.trim().replaceAll("\\]\\[", ",").replaceFirst("\\[", "").replaceFirst("\\]", "");
		return ids;
	}

	
	/**
	 * 获取下一个孩子的数字编号code值
	 * 例如：父Code=001 其最大编号的儿子是001006，则num=3,返回001007
	 * @param pCode 	父亲的Code
	 * @param cCode 	孩子中最大的Code
	 * @return		  	返回下一个Code值
	 */
	public static String getNextCode(String pCode, String cCode) {
		cCode+="";
		// 初始化，nextCode等于父Code
		String nextCode = pCode;
		// 获得编号分段长度
		int num = pCode.length() - cCode.length();
		
		// 将编号转为数字
		int tempCode = 1;
		cCode = cCode.substring(cCode.length() - num);
		tempCode = Integer.valueOf(cCode)+1;
		
		// 如果+1后溢出边界，则-1改回边界值
		if(String.valueOf(tempCode).length() > num) {
			tempCode = tempCode-1;
			nextCode = nextCode + tempCode;
		}
		// 补0
		else{
			for(int i=String.valueOf(tempCode).length(); i<num; i++) {
				nextCode += "0";
			}
			nextCode = nextCode + tempCode;
		}
		
		return nextCode;
	}
	
	
	/**
	 * 用0为ID补齐长度，并返回字符串
	 * @param id		ID值
	 * @param length	补齐到的长度
	 */
	public static String getStrId(Integer id, int length){
		String str = id.toString();
		
		if(str.length() == length) {
			for(int i=0; i<length; i++) {
				str += "0";
			}
		}
		else{
			String str1 = "";
			for(int i=str.length(); i<length; i++) {
				str1 += "0";
			}
			str = str1 + str;
		}
		
		return str;
	}
	
	
	/**
	 * 验证邮箱地址是否正确
	 * @param email
	 * @return
	 */
	public static boolean isEmail(String email) {
		boolean flag = false;
		try{
			String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(email);
			flag = matcher.matches();
		} catch(Exception e){
			flag = false;
		}
		return flag;
	}
	
	
	/**
	 * 验证手机号码
	 * @param mobile
	 * @return
	 */
	public static boolean isMobileNO(String mobile) {
		boolean flag = false;
		try{
			Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
			Matcher m = p.matcher(mobile);
			flag = m.matches();
		} catch(Exception e){
	   		flag = false;
		}
		return flag;
	}
	
	
	/**
     * 验证身份证号码
     * @param idCard 居民身份证号码15位或18位，最后一位可能是数字或字母
     * @return 验证成功返回true，验证失败返回false
     */ 
    public static boolean isIdCard(String idCard) {
    	boolean flag = false;
    	try {
    		Pattern p = Pattern.compile("[1-9]\\d{13,16}[a-zA-Z0-9]{1}");
			Matcher m = p.matcher(idCard);
			flag = m.matches();
    	} catch(Exception e){
			flag = false;
		}
    	return flag;
    }
    
    
    public static boolean isHttpUrl(String url) {
    	if(url==null || url.equals("")) {
    		return false;
    	}
    	
    	String regex = "(http:|https:)//[^[A-Za-z0-9\\._\\?%&+\\-=/#]]*";
    	Pattern pattern = Pattern.compile(regex);
    	Matcher matcher = pattern.matcher(url);
    	
    	if(matcher.find()) {
    		return url.trim().equalsIgnoreCase(matcher.group());
    	}
    	else {
    		return false;
    	}
    }
    
    
	
	/**
	 * 传入一个字符串，自动识别是身份证、邮箱或是手机号，然后根据情况隐藏字符串，隐藏字符用'*'号代替
	 * @param str	传入字符串
	 * @return
	 */
	public static String hideString(String str) {
		str += "";
		// 如果是邮箱
		if(isEmail(str)) {
			// 将开头第三个到@之前的字符隐藏掉
			return str.substring(0, 2) + "****" + str.substring(str.indexOf("@"));
		} 
		else if(isMobileNO(str)) {
			// 将开头3个与后3个之前的数字隐藏掉
			return str.substring(0, 3) + "*****" + str.substring(8);
		} 
		else if(isIdCard(str)) {
			// 除开头6位和倒数3、4位以外，全隐藏
			if(str.length()==15) {
				return str.substring(0, 6) + "******" + str.substring(12, 14) + "*";
			}
			else {
				return str.substring(0, 6) + "********" + str.substring(14, 16) + "**";
			}
		}
		else {
			return "******";
		}
	}
	
	/**
	 * 传入一个字符串，判断是否为纯数字
	 * @param str	传入字符串
	 * @return
	 */
	public static boolean isNumber(String str){
		boolean flag = false;
    	try {
    		Pattern p = Pattern.compile("[0-9]*");
			Matcher m = p.matcher(str);
			flag = m.matches();
    	} catch(Exception e){
			flag = false;
		}
    	return flag;
	}
	
    public static String delHTMLTag(String htmlStr){
         String regEx_script="<script[^>]*?>[\\s\\S]*?<\\/script>"; //定义script的正则表达式
         String regEx_style="<style[^>]*?>[\\s\\S]*?<\\/style>"; //定义style的正则表达式
         String regEx_html="<[^>]+>"; //定义HTML标签的正则表达式
         Pattern p_script=Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE);
         Matcher m_script=p_script.matcher(htmlStr);
         htmlStr=m_script.replaceAll("");//过滤script标签
         Pattern p_style=Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE);
         Matcher m_style=p_style.matcher(htmlStr);
         htmlStr=m_style.replaceAll(""); //过滤style标签
         Pattern p_html=Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE);
         Matcher m_html=p_html.matcher(htmlStr);
         htmlStr=m_html.replaceAll(""); //过滤html标签
         return htmlStr.trim(); //返回文本字符串
     }
    
    /**
     * 截取字符串中的所有image路径
     * @param str
     * @return
     */
    public static List<String> imgCut(String str){
    	str = str.trim();
    	String[] b = str.split("src=\"");
    	
    	List<String> list = new ArrayList<String>();
    	
    	for (int i = 1; i < b.length; i++) {
    		String[] c = b[i].split("\"");
    		list.add(c[0]);
    	}
    	
    	return list;
    }
    
    /**
     * 截取字符串中的所有src路径
     * @param str
     * @return
     */
    public static List<String> embCut(String str){
    	str = str.trim();
    	String[] b = str.split("src=\"");
    	
    	List<String> list = new ArrayList<String>();
    	
    	for (int i = 1; i < b.length; i++) {
    		String[] c = b[i].split("\"");
    		if(c[0].startsWith("http:") &&c[0].contains(".swf")){
    			if(c[0].indexOf("player.ku6") == -1 && c[0].indexOf("static.video.qq") == -1){
    				list.add(c[0]);
    			}
    		}
    	}
    	
    	return list;
    }
    
    /**
     * 截取字符串中的所有image路径
     * @param str
     * @return
     */
    public static  List<String> imgHtmlCut(String str){
    	str = str.trim();
    	String[] b = str.split("<img\"");
    	
    	List<String> list = new ArrayList<String>();
    	
    	for (int i = 1; i < b.length; i++) {
    		String[] c = b[i].split("\"");
    		list.add(c[0]);
    	}
    	
    	return list;
    }
    
    /**
     * 转义html标签
     * @param str html内容
     * @return
     */
    public static String escapeHTML(String str) {
    	return StringUtils.replaceEach(str, new String[]{"&", "\"", "<", ">"}, new String[]{"&amp;", "&quot;", "&lt;", "&gt;"});
    }
    
}


