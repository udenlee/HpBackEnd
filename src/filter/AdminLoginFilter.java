package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AdminLoginFilter implements Filter{
	private final String ADMIN_LOGIN_URI = "/admin/login"; 

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session = request.getSession();
		Object adminUser = session.getAttribute("admin");
		String requestURI = request.getRequestURI();
		// TODO 限制ip端
		// 已经登录跳后台首页
		if (adminUser != null && ADMIN_LOGIN_URI.equals(requestURI)) {
			response.sendRedirect("/admin/index");
			return;
		}
//		// 未登录跳登录页
		if (null == adminUser && !ADMIN_LOGIN_URI.equals(requestURI)) {
			response.sendRedirect("/admin/login");
			return;
		} 
		
		chain.doFilter(request, response);
	}
	
	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}
