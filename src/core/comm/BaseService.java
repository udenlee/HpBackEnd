package core.comm;

import java.util.List;

import javax.annotation.Resource;


public class BaseService<T> {
	private BaseDao<T> baseDao;
	@Resource
	public void setBaseDao(BaseDao<T> baseDao) {
		this.baseDao = baseDao;
	}
	
	public void isSave(T object) {
		baseDao.save(object);
	}
	public void isUpdate(T object) {
		baseDao.update(object);
	}
	public int isUpdate(List<Propertie> props) {
		return baseDao.update(props);
	}

	/** 修改 */
	
	public void isDelete(T object) {
		baseDao.delete(object);
	}
	
	public void isDelete(Long objectId) {
		baseDao.delete(objectId);
	}
	
	public void isDelete(Integer objectId) {
		baseDao.delete(objectId);
	}
	
	public void isDelete(String objectUuid) {
		baseDao.delete(objectUuid);
	}
	
	public void isDelete(List<Propertie> props) {
		baseDao.delete(props);
	}

	/** 查找 */
	
	public T findById(Long objectId) {
		return baseDao.find(objectId);
	}
	
	public T findById(Integer objectId) {
		return baseDao.find(objectId);
	}
	
	public T findById(String objectUuid) {
		return baseDao.find(objectUuid);
	}
	
	public T findByParam(List<Propertie> props) {
		return baseDao.find(props);
	}
	
	/** 查询 */
	
	public long findSum(String sumColumn, List<Propertie> props) {
		return baseDao.findSum(sumColumn, props);
	}
	
	public long findMax(String maxColumn, List<Propertie> props) {
		return baseDao.findMax(maxColumn, props);
	}
	
	public long findCount(List<Propertie> props) {
		return baseDao.findCount(props);
	}
	
	public List<T> findList(List<Propertie> props, int page, int limit) {
		return baseDao.findList(props, page, limit, null);
	}
	
	public List<T> findList(List<Propertie> props, int page, int limit, String sort) {
		return baseDao.findList(props, page, limit, sort);
	}
	
	public List<Object> findGroup(List<Propertie> props, int page, int limit) {
		return baseDao.findGroup(props, page, limit);
	}
	
	public T queryById(Long objectId) {
		return baseDao.query(objectId);
	}
	
	public T queryById(Integer objectId) {
		return baseDao.query(objectId);
	}
	
	public T queryById(String objectUuid) {
		return baseDao.query(objectUuid);
	}
	
	public T queryByParam(List<Propertie> props) {
		return baseDao.query(props);
	}
	
	/** 从库查询 */
	
	public long querySum(String sumColumn, List<Propertie> props) {
		return baseDao.querySum(sumColumn, props);
	}
	
	public long queryMax(String maxColumn, List<Propertie> props) {
		return baseDao.queryMax(maxColumn, props);
	}
	
	public long queryCount(List<Propertie> props) {
		return baseDao.queryCount(props);
	}
	
	public List<T> queryList(List<Propertie> props, int page, int limit, String sort) {
		return baseDao.queryList(props, page, limit, sort);
	}
	
	public List<Object> queryGroup(List<Propertie> props, int page, int limit, String sort) {
		return baseDao.queryGroup(props, page, limit, sort);
	}

}
