package core.comm;


/**
 * 运行时异常
 * @author 韩晗		2014-01-12
 */
@SuppressWarnings("serial")
public class MyException extends RuntimeException {

	public MyException() {
		super();
	}

	public MyException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MyException(String message, Throwable cause) {
		super(message, cause);
	}

	public MyException(String message) {
		super(message);
	}

	public MyException(Throwable cause) {
		super(cause);
	}

}
