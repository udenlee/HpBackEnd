package core.comm;


/**
 * 
 * @author Administrator
 * 更新，查询 数据库的时候，通用的表示类
 * 
 */
public class Propertie {
	
	
	private String type="where";	// 参数类型 set/where
	private String name;			// 属性名
	private String operator="";		// 运算符
	private Object value;			// 运算值

	
	public Propertie() {
	}
	public Propertie(String name, String operator, Object value) {
		this.name=name;
		this.operator=operator;
		this.value=value;
	}
	public Propertie(String type, String name, String operator, Object value) {
		this.type=type;
		this.name=name;
		this.operator=operator;
		this.value=value;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}

}
