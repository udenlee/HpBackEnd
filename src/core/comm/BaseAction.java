package core.comm;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import config.GlobalConfig;
import core.admin.location.service.ContentService;
import core.admin.location.service.LocationService;
import entity.Content;
import entity.Location;
import util.AllUtil;
import util.BeanUtil;
import util.JSONUtil;

@SuppressWarnings("serial")
@Controller("baseAction")
@Scope("prototype")
public class BaseAction extends ActionSupport{
	private File file;
	private String fileFileName;
	protected int uploadLocation;	// 图片上传目录分类编号
	protected Map<String, Object> results = new HashMap<>();
	/** 容器 */
	protected HttpServletRequest request = ServletActionContext.getRequest();
	protected HttpServletResponse response = ServletActionContext.getResponse();
	protected final Map<String, Object> session = ActionContext.getContext().getSession();
	protected final Map<String, Object> context = ActionContext.getContext().getApplication();
	protected int id;
	protected int page = 1;
	protected int limit = 10;
	protected Long count;
	protected String sort;
	@Resource
	private LocationService locationService;
	@Resource
	private ContentService contentService;
	
	/**
	 * 上传 
	 * @return
	 * @author 王涛
	 * 2016年1月28日
	 */
	public void upload () {
		long size = 2048000;
		// 上传图片限制大小
		if (file.length() > size) {
			results.put("err", "文件太大");
			JSONUtil.responseJSON(results);
			return;
		}
		// 保存文件
		// 图片名称为UUID
		UUID uuid = UUID.randomUUID();
		// 文件扩展名
		String ext = fileFileName.substring(fileFileName.lastIndexOf("."), fileFileName.length());
		// 文件名重写
		fileFileName = uuid + ext;
		// 要存到数据库中的文件路径
		String dbPath = "/upload/" + fileFileName;
		// 文件存放目录
		String dirPath = GlobalConfig.SERVER_PATH + "/upload";
		dirPath = dirPath.replace("/", String.valueOf(File.separatorChar));
		// 上传图片
		String errorInfo = AllUtil.upload(file, dirPath, fileFileName);
		results.put("err", errorInfo);
		results.put("url", dbPath);
		JSONUtil.responseJSON(results);
	}
	
	public Boolean isAjax() {
		return "XMLHttpRequest".equalsIgnoreCase(request
				.getHeader("X-Requested-With"));
	}
	
	public boolean isGet() {
		return request.getMethod().toUpperCase().equals("GET");
	}
	
	public boolean isPost() {
		return request.getMethod().toUpperCase().equals("POST");
	}
	
	public String sidebar () {
		// 查询通用位置信息
		loadLocationContent(7);
		return SUCCESS;
	}
	
	protected void assembleResults(List<?> list) {
		results.put("page", page);
		results.put("limit", limit);
		results.put("totalCount", count);
		results.put("items", list);
		JSONUtil.responseJSON(results);
	}
	
	// 查询位置信息
	protected void loadLocationContent(int page_id) {
		List<Propertie> propsLocation = new ArrayList<Propertie>();
		propsLocation.add(new Propertie("where", "page_id", "=", page_id));
		// 查询该页面下所有位置信息
		List<Location> locationList = locationService.findList(propsLocation, 0, 0);
		for (int i = 0; i < locationList.size(); i++) {
			Location location = locationList.get(i);
			Content content = contentService.findByLocationId(location.getId());
			Map<String, Object> allMap = new HashMap<>();
			if (content != null) {
				allMap.putAll(BeanUtil.convertObjectToMap(content));
			}
			if (location != null) {
				allMap.putAll(BeanUtil.convertObjectToMap(location));
			}
			results.put(location.getCode(), allMap);
		}
	}
	
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public Map<String, Object> getResults() {
		return results;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public void setFile(File file) {
		this.file = file;
	}
	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}
	public File getFile() {
		return file;
	}
	public String getFileFileName() {
		return fileFileName;
	}
	public void setUploadLocation(int uploadLocation) {
		this.uploadLocation = uploadLocation;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
