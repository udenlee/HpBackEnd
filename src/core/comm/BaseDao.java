package core.comm;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import util.AllUtil;
import util.HqlUtil;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class BaseDao<T> {

	protected Class<T> clazz;

	@Resource
	public SessionFactory sessionFactory0;

	// 无参构造方法
	public BaseDao() {
		// 获得超类的泛型参数的实际类型
		// 表示此Class所表示的实体（类、接口、基本类型或 void）的直接超类的 Type然后将其转换ParameterizedType
		// getActualTypeArguments()[0]:表示此类型实际类型参数的 Type 对象的数组
		this.clazz = ((Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
	}

	public void save(T object) {
		sessionFactory0.getCurrentSession().save(object);
	}

	public void delete(Integer objectId) {
		delete(find(objectId));
	}

	public void delete(Long objectId) {
		delete(find(objectId));
	}

	public void delete(String objectUuid) {
		delete(find(objectUuid));
	}

	public void delete(T object) {
		sessionFactory0.getCurrentSession().delete(object);
	}

	public void update(T object) {
		sessionFactory0.getCurrentSession().merge(object);
	}

	public int update(List<Propertie> prots) {
		String hql = "update " + clazz.getName() + " set " + HqlUtil.getHqlSetters(prots);
		if (!HqlUtil.getHqlFinders(prots).equals("")) {
			hql += " where " + HqlUtil.getHqlFinders(prots);
		}

		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		int indexSeter = 1;
		int indexFinder = 1;
		for (Propertie prot : prots) {
			if (!"".equals(prot.getOperator())) {
				if ("set".equals(prot.getType())) {
					query.setParameter("s" + indexSeter, prot.getValue());
					indexSeter++;
				} else if ("where".equals(prot.getType())) {
					if (prot.getOperator().indexOf("in") != -1) {
						List<Integer> intList = new ArrayList<Integer>();
						String[] tempArray = String.valueOf(prot.getValue()).split(",");
						for (String var : tempArray) {
							intList.add(Integer.valueOf(var));
						}
						query.setParameterList("f" + indexFinder, intList);
						indexFinder++;
					} else {
						query.setParameter("f" + indexFinder, prot.getValue());
						indexFinder++;
					}
				}
			}
		}

		return query.executeUpdate();
	}

	public T find(Integer objectId) {
		if (null != objectId) {
			return (T) sessionFactory0.getCurrentSession().get(clazz, objectId);
		}
		return null;
	}

	public T find(Long objectId) {
		if (null != objectId) {
			return (T) sessionFactory0.getCurrentSession().get(clazz, objectId);
		}
		return null;
	}

	public T find(String objectUuid) {
		if (null != objectUuid) {
			return (T) sessionFactory0.getCurrentSession().get(clazz, objectUuid);
		}
		return null;
	}

	public T find(List<Propertie> prots) {
		String hql = " from " + clazz.getName();
		if (prots != null && prots.size() > 0) {
			hql += " where " + HqlUtil.getHqlFinders(prots);
		}
		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		int indexFinder = 1;
		for (Propertie prot : prots) {
			if (!"".equals(prot.getOperator())) {
				if (prot.getOperator().indexOf("in") != -1) {
					List<Integer> intList = new ArrayList<Integer>();
					String[] tempArray = String.valueOf(prot.getValue()).split(",");
					for (String var : tempArray) {
						intList.add(Integer.valueOf(var));
					}
					query.setParameterList("f" + indexFinder, intList);
					indexFinder++;
				} else {
					query.setParameter("f" + indexFinder, prot.getValue());
					indexFinder++;
				}
			}
		}
		return (T) query.uniqueResult();
	}

	public List<T> findList(List<Propertie> prots, int page, int limit, String sort) {
		String hql = "from " + clazz.getName();
		if (prots != null && prots.size() > 0) {
			hql += " where " + HqlUtil.getHqlFinders(prots);
		}
		if (!AllUtil.isNull(sort)) {
			hql += " order by " + sort;
		}

		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		int indexFinder = 1;
		if (prots != null && prots.size() > 0) {
			for (Propertie prot : prots) {
				if (!"".equals(prot.getOperator())) {
					if (prot.getOperator().indexOf("in") != -1) {
						List<Integer> intList = new ArrayList<Integer>();
						String[] tempArray = String.valueOf(prot.getValue()).split(",");
						for (String var : tempArray) {
							intList.add(Integer.valueOf(var));
						}
						query.setParameterList("f" + indexFinder, intList);
						indexFinder++;
					} else {
						query.setParameter("f" + indexFinder, prot.getValue());
						indexFinder++;
					}
				}
			}
		}

		if (page <= 0) {
			page = 1;
		}
		if (limit > 0) {
			query.setFirstResult((page - 1) * limit).setMaxResults(limit);
		} else {
			query.setFirstResult((page - 1) * limit);
		}
		List<T> list = (List<T>) query.list();
		return list;
	}

	public Integer findCount(List<Propertie> prots) {
		String hql = "select count(*) from " + clazz.getName();
		if (prots != null && prots.size() > 0) {
			hql += " where " + HqlUtil.getHqlFinders(prots);
		}

		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		int indexFinder = 1;
		for (Propertie prot : prots) {
			if (!"".equals(prot.getOperator())) {
				if (prot.getOperator().indexOf("in") != -1) {
					List<Integer> intList = new ArrayList<Integer>();
					String[] tempArray = String.valueOf(prot.getValue()).split(",");
					for (String var : tempArray) {
						intList.add(Integer.valueOf(var));
					}
					query.setParameterList("f" + indexFinder, intList);
					indexFinder++;
				} else {
					query.setParameter("f" + indexFinder, prot.getValue());
					indexFinder++;
				}
			}
		}
		Integer count = ((Long) query.uniqueResult()).intValue();
		return count;
	}

	public Long findSum(String sumColumn, List<Propertie> prots) {
		String hql = "select Sum(" + sumColumn + ") from " + clazz.getName();
		if (prots != null && prots.size() > 0) {
			hql += " where " + HqlUtil.getHqlFinders(prots);
		}

		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		int indexFinder = 1;
		for (Propertie prot : prots) {
			if (!"".equals(prot.getOperator())) {
				if (prot.getOperator().indexOf("in") != -1) {
					List<Integer> intList = new ArrayList<Integer>();
					String[] tempArray = String.valueOf(prot.getValue()).split(",");
					for (String var : tempArray) {
						intList.add(Integer.valueOf(var));
					}
					query.setParameterList("f" + indexFinder, intList);
					indexFinder++;
				} else {
					query.setParameter("f" + indexFinder, prot.getValue());
					indexFinder++;
				}
			}
		}

		Number sum = (Number) query.uniqueResult();
		if (sum == null) {
			return 0L;
		} else {
			return sum.longValue();
		}
	}

	public Long findMax(String maxColumn, List<Propertie> prots) {
		String hql = "select Max(" + maxColumn + ") from " + clazz.getName();
		if (prots != null && prots.size() > 0) {
			hql += " where " + HqlUtil.getHqlFinders(prots);
		}

		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		int indexFinder = 1;
		for (Propertie prot : prots) {
			if (!"".equals(prot.getOperator())) {
				if (prot.getOperator().indexOf("in") != -1) {
					List<Integer> intList = new ArrayList<Integer>();
					String[] tempArray = String.valueOf(prot.getValue()).split(",");
					for (String var : tempArray) {
						intList.add(Integer.valueOf(var));
					}
					query.setParameterList("f" + indexFinder, intList);
					indexFinder++;
				} else {
					query.setParameter("f" + indexFinder, prot.getValue());
					indexFinder++;
				}
			}
		}

		Number sum = (Number) query.uniqueResult();
		if (sum == null) {
			return 0L;
		} else {
			return sum.longValue();
		}
	}

	public T query(Integer objectId) {
		if (null != objectId) {
			return (T) sessionFactory0.getCurrentSession().get(clazz, objectId);
		}
		return null;
	}

	public T query(Long objectId) {
		if (null != objectId) {
			return (T) sessionFactory0.getCurrentSession().get(clazz, objectId);
		}
		return null;
	}

	public T query(String objectUuid) {
		if (null != objectUuid) {
			return (T) sessionFactory0.getCurrentSession().get(clazz, objectUuid);
		}
		return null;
	}

	public T query(List<Propertie> prots) {
		String hql = " from " + clazz.getName();
		if (prots != null && prots.size() > 0) {
			hql += " where " + HqlUtil.getHqlFinders(prots);
		}
		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		int indexFinder = 1;
		for (Propertie prot : prots) {
			if (!"".equals(prot.getOperator())) {
				if (prot.getOperator().indexOf("in") != -1) {
					List<Integer> intList = new ArrayList<Integer>();
					String[] tempArray = String.valueOf(prot.getValue()).split(",");
					for (String var : tempArray) {
						intList.add(Integer.valueOf(var));
					}
					query.setParameterList("f" + indexFinder, intList);
					indexFinder++;
				} else {
					query.setParameter("f" + indexFinder, prot.getValue());
					indexFinder++;
				}
			}
		}
		return (T) query.uniqueResult();
	}

	public List<T> queryList(List<Propertie> prots, int page, int limit, String sort) {
		String hql = "from " + clazz.getName();
		if (prots != null && prots.size() > 0) {
			hql += " where " + HqlUtil.getHqlFinders(prots);
		}
		if (!AllUtil.isNull(sort)) {
			hql += " order by " + sort;
		}

		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		int indexFinder = 1;
		for (Propertie prot : prots) {
			if (!"".equals(prot.getOperator())) {
				if (prot.getOperator().indexOf("in") != -1) {
					List<Integer> intList = new ArrayList<Integer>();
					String[] tempArray = String.valueOf(prot.getValue()).split(",");
					for (String var : tempArray) {
						intList.add(Integer.valueOf(var));
					}
					query.setParameterList("f" + indexFinder, intList);
					indexFinder++;
				} else {
					query.setParameter("f" + indexFinder, prot.getValue());
					indexFinder++;
				}
			}
		}
		if (page <= 0) {
			page = 1;
		}
		if (limit > 0) {
			query.setFirstResult((page - 1) * limit).setMaxResults(limit);
		} else {
			query.setFirstResult((page - 1) * limit);
		}
		List<T> list = (List<T>) query.list();
		return list;
	}

	public Integer queryCount(List<Propertie> prots) {
		String hql = "select count(*) from " + clazz.getName();
		if (prots != null && prots.size() > 0) {
			hql += " where " + HqlUtil.getHqlFinders(prots);
		}

		Session session = sessionFactory0.openSession();
		Query query = session.createQuery(hql);
		int indexFinder = 1;
		for (Propertie prot : prots) {
			if (!"".equals(prot.getOperator())) {
				if (prot.getOperator().indexOf("in") != -1) {
					List<Integer> intList = new ArrayList<Integer>();
					String[] tempArray = String.valueOf(prot.getValue()).split(",");
					for (String var : tempArray) {
						intList.add(Integer.valueOf(var));
					}
					query.setParameterList("f" + indexFinder, intList);
					indexFinder++;
				} else {
					query.setParameter("f" + indexFinder, prot.getValue());
					indexFinder++;
				}
			}
		}
		Integer count = ((Long) query.uniqueResult()).intValue();
		return count;
	}

	public Long querySum(String sumColumn, List<Propertie> prots) {
		String hql = "select Sum(" + sumColumn + ") from " + clazz.getName();
		if (prots != null && prots.size() > 0) {
			hql += " where " + HqlUtil.getHqlFinders(prots);
		}
		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		int indexFinder = 1;
		for (Propertie prot : prots) {
			if (!"".equals(prot.getOperator())) {
				if (prot.getOperator().indexOf("in") != -1) {
					List<Integer> intList = new ArrayList<Integer>();
					String[] tempArray = String.valueOf(prot.getValue()).split(",");
					for (String var : tempArray) {
						intList.add(Integer.valueOf(var));
					}
					query.setParameterList("f" + indexFinder, intList);
					indexFinder++;
				} else {
					query.setParameter("f" + indexFinder, prot.getValue());
					indexFinder++;
				}
			}
		}
		Number sum = (Number) query.uniqueResult();
		if (sum == null) {
			return 0L;
		} else {
			return sum.longValue();
		}
	}

	public Long queryMax(String maxColumn, List<Propertie> prots) {
		String hql = "select Max(" + maxColumn + ") from " + clazz.getName();
		if (prots != null && prots.size() > 0) {
			hql += " where " + HqlUtil.getHqlFinders(prots);
		}
		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		int indexFinder = 1;
		for (Propertie prot : prots) {
			if (!"".equals(prot.getOperator())) {
				if (prot.getOperator().indexOf("in") != -1) {
					List<Integer> intList = new ArrayList<Integer>();
					String[] tempArray = String.valueOf(prot.getValue()).split(",");
					for (String var : tempArray) {
						intList.add(Integer.valueOf(var));
					}
					query.setParameterList("f" + indexFinder, intList);
					indexFinder++;
				} else {
					query.setParameter("f" + indexFinder, prot.getValue());
					indexFinder++;
				}
			}
		}
		Number sum = (Number) query.uniqueResult();
		if (sum == null) {
			return 0L;
		} else {
			return sum.longValue();
		}
	}

	public void delete(List<Propertie> props) {
		String hql = "delete from " + clazz.getName() + " where " + HqlUtil.getHqlFinders(props);
		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		query = AllUtil.modifyQuery(query, props);
		query.executeUpdate();
	}

	public List<Object> findGroup(List<Propertie> props, int page, int limit) {
		String hql = "select * from " + clazz.getName();
		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		query = AllUtil.modifyQuery(query, props, page, limit);
		return query.list();
	}
	
	public List<Object> findGroup(List<Propertie> props, int page, int limit, String sort) {
		String hql = "select * from " + clazz.getName();
		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		query = AllUtil.modifyQuery(query, props, page, limit);
		return query.list();
	}
	
	public List<Object> queryGroup(List<Propertie> props, int page, int limit) {
		String hql = "select * from " + clazz.getName();
		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		query = AllUtil.modifyQuery(query, props, page, limit);
		return query.list();
	}
	
	public List<Object> queryGroup(List<Propertie> props, int page, int limit, String sort) {
		String hql = "select * from " + clazz.getName();
		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		query = AllUtil.modifyQuery(query, props, page, limit);
		return query.list();
	}
}
