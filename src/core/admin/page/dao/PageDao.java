package core.admin.page.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import core.comm.BaseDao;
import core.comm.Propertie;
import entity.Page;
import entity.vo.PageVo;
import util.AllUtil;
import util.HqlUtil;

@Repository("pageDao")
public class PageDao extends BaseDao<Page>{
	@SuppressWarnings("unchecked")
	public List<PageVo> findVoList (List<Propertie> props, int page, int limit) {
		String hql = "from PageVo ";
		if (props.size() > 0) {
			hql += " where " + HqlUtil.getHqlFinders(props);
		}
		Query query = sessionFactory0.getCurrentSession().createQuery(hql);
		AllUtil.modifyQuery(query, props, page, limit);
		List<PageVo> list = query.list();
		return list;
	}
}
