package core.admin.page.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import core.admin.page.service.PageService;
import core.comm.BaseAction;
import core.comm.Propertie;
import entity.Page;

@SuppressWarnings("serial")
@Controller("pageAction")
@Scope("prototype")
public class PageAction extends BaseAction{
	
	@Resource
	private PageService pageService;
	
	public void listPage () {
		List<Propertie> props = new ArrayList<Propertie>();
		List<Page> list = pageService.findList(props, 0, 0);
		assembleResults(list);
	}
}
