package core.admin.article.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import core.admin.article.dao.ArticleDao;
import core.comm.BaseDao;
import core.comm.BaseService;
import core.comm.Propertie;
import entity.Article;

@Service("articleService")
public class ArticleService extends BaseService<Article>{
	
	
	@Resource
	private ArticleDao articleDao;
	
	@Resource(name = "articleDao")
	public void setBaseDao(BaseDao<Article> dao) {
		super.setBaseDao(dao);
	}
	
	public Article findNext (int id) {
		List<Propertie> props = new ArrayList<Propertie>();
		props.add(new Propertie("where", "id", ">", id));
		List<Article> articles = articleDao.findList(props, 1, 1, "id asc");
		if (articles.size() != 0) {
			return articles.get(0);
		}
		props.clear();
		articles = articleDao.findList(props, 1, 1, "id desc");
		if (articles.size() != 0) {
			return articles.get(0);
		}
		return null;
	}
	
	public Article findPrev (int id) {
		List<Propertie> props = new ArrayList<Propertie>();
		props.add(new Propertie("where", "id", "<", id));
		List<Article> articles = articleDao.findList(props, 1, 1, "id asc");
		if (articles.size() != 0) {
			return articles.get(0);
		}
		props.clear();
		articles = articleDao.findList(props, 1, 1, "id desc");
		if (articles.size() != 0) {
			return articles.get(0);
		}
		return null;
	}
}
