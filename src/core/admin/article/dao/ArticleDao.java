package core.admin.article.dao;

import org.springframework.stereotype.Repository;

import core.comm.BaseDao;
import entity.Article;

@Repository("articleDao")
public class ArticleDao extends BaseDao<Article>{
}
