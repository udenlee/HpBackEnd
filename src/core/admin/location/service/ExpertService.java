package core.admin.location.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import core.admin.location.dao.ExpertDao;
import core.comm.BaseDao;
import core.comm.BaseService;
import entity.Expert;

@Service("expertService")
public class ExpertService extends BaseService<Expert>{
	@Resource
	private ExpertDao expertDao;
	
	@Resource(name = "expertDao")	
	@Override
	public void setBaseDao(BaseDao<Expert> baseDao) {
		super.setBaseDao(baseDao);
	}
}
