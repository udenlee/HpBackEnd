package core.admin.location.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import core.admin.location.dao.LocationDao;
import core.comm.BaseDao;
import core.comm.BaseService;
import entity.Location;

@Service("locationService")
public class LocationService extends BaseService<Location>{
	@Resource
	private LocationDao locationDao;
	
	@Resource(name = "locationDao")
	@Override
	public void setBaseDao(BaseDao<Location> baseDao) {
		super.setBaseDao(baseDao);
	}
}
