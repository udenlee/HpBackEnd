package core.admin.location.dao;

import org.springframework.stereotype.Repository;

import core.comm.BaseDao;
import entity.Office;

@Repository("officeDao")
public class OfficeDao extends BaseDao<Office>{

}
