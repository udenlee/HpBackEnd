package core.admin.location.dao;

import org.springframework.stereotype.Repository;

import core.comm.BaseDao;
import entity.Location;

@Repository("locationDao")
public class LocationDao extends BaseDao<Location>{

}
