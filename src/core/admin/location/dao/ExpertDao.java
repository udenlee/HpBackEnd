package core.admin.location.dao;

import org.springframework.stereotype.Repository;

import core.comm.BaseDao;
import entity.Expert;


@Repository("expertDao")
public class ExpertDao extends BaseDao<Expert>{

}
