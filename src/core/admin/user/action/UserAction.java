package core.admin.user.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import core.admin.user.service.UserService;
import core.comm.BaseAction;
import core.comm.Propertie;
import entity.User;

@SuppressWarnings("serial")
@Controller("userAction")
@Scope("prototype")
public class UserAction extends BaseAction{
	
	@Resource
	private UserService userService;
	
	/**
	 * 用户列表 
	 * @author 王涛
	 * 2016年2月4日
	 */
	public void listUser () {
		List<Propertie> props = new ArrayList<Propertie>();
		List<User> userList = userService.findList(props, page, limit, "id desc");
		count = userService.findCount(props);
		List<User> voList = new ArrayList<>();
		for (User tmp: userList) {
			User voUser = new User();
			BeanUtils.copyProperties(tmp, voUser);
			voUser.setPassword(null);
			voList.add(voUser);
		}
		assembleResults(voList);
	}
}
