package core.admin.user.action;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import core.admin.user.service.UserService;
import core.comm.BaseAction;
import entity.User;
import util.AllUtil;
import util.EncryptUtil;
import util.JSONUtil;

@Controller("loginAction")
@Scope("prototype")
@SuppressWarnings("serial")
public class LoginAction extends BaseAction{
	
	@Resource
	private UserService userService;
	
	private String username;
	private String password;
	private String vcode;
	
	/**
	 * 后台登录 
	 * @author 王涛
	 * 2016年3月24日
	 */
	public String adminLogin () {
		if (!isPost()) {
			return SUCCESS;
		}
		
		String answer = (String) session.get("captcha");
		session.remove("captcha");
		
		// 判断用户名密码
		if (!AllUtil.AND(username, password)) {
			results.put("err", "请填写用户名及密码");
			JSONUtil.responseJSON(results);
			return NONE;
		}
		
		if (!AllUtil.AND(vcode, answer)) {
			results.put("err", "请填写验证码");
			JSONUtil.responseJSON(results);
			return NONE;
		}
		
		if (!vcode.equals(answer)) {
			results.put("err", "验证码不正确");
			JSONUtil.responseJSON(results);
			return NONE;
		}
		
		User user = userService.findByUsername(username);
		if (user != null && user.getPassword().equals(EncryptUtil.encodeByMD5(password))) {
			session.put("admin", user);
		} else {
			results.put("err", "用户名或密码错误");
		}
		
		JSONUtil.responseJSON(results);
		return NONE;
	}
	
	/**
	 * 退出登录 
	 * @author 王涛
	 * 2016年4月11日
	 */
	public String logout () {
		session.clear();
		return SUCCESS;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
}
