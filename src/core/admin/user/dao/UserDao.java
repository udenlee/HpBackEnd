package core.admin.user.dao;

import org.springframework.stereotype.Repository;

import core.comm.BaseDao;
import entity.User;

@Repository("userDao")
public class UserDao extends BaseDao<User>{

}
