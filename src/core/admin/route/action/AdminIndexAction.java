package core.admin.route.action;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import core.comm.BaseAction;

@SuppressWarnings("serial")
@Controller("adminIndexAction")
@Scope("prototype")
public class AdminIndexAction extends BaseAction {
	
	public String index () {
		return SUCCESS;
	}
}
