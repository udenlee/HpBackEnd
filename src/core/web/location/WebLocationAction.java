package core.web.location;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import core.admin.location.service.ContentService;
import core.admin.location.service.LocationService;
import core.comm.BaseAction;
import entity.Content;
import entity.Location;


@Controller()
@Scope("prototype")
@SuppressWarnings("serial")
public class WebLocationAction extends BaseAction {
	@Resource
	private LocationService locationService;
	@Resource
	private ContentService contentService;
	
	private Location location;
	private Content content;
	
	/**
	 * 查询位置信息
	 * @author 王涛
	 * 2016年4月1日
	 */
	public String locationInfo () {
		location = locationService.findById(id);
		if (location != null) {
			content = contentService.findByLocationId(id);
		}
		return SUCCESS;
	}
	public Location getLocation() {
		return location;
	}
	public Content getContent() {
		return content;
	}
}
