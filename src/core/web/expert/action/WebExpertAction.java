package core.web.expert.action;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import core.admin.location.service.ExpertService;
import core.comm.BaseAction;
import entity.Expert;


@Controller()
@Scope("prototype")
@SuppressWarnings("serial")
public class WebExpertAction extends BaseAction {
	
	@Resource
	private ExpertService expertService;
	/**
	 * 专家内页 
	 * @author 王涛
	 * 2016年3月30日
	 */
	public String expertIndex () {
		Expert expert = expertService.findById(id);
		if (expert == null) {
			return ERROR;
		}
		
		results.put("expert", expert);
		return SUCCESS;
	}
}
