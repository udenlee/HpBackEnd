package core.web.news;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import core.admin.article.service.ArticleService;
import core.comm.BaseAction;
import entity.Article;

@SuppressWarnings("serial")
@Controller()
@Scope("prototype")
public class WebNewsAction extends BaseAction{
	
	@Resource
	private ArticleService articleService;
	
	public String newsContent () {
		Article article = articleService.findById(id);
		if (article == null) {
			return ERROR;
		}
		results.put("article", article);
		Article nextOne = articleService.findNext(id);
		Article prevOne = articleService.findPrev(id);
		results.put("next", nextOne);
		results.put("prev", prevOne);
		return SUCCESS;
	}
}
