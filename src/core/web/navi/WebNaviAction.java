package core.web.navi;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import core.admin.article.service.ArticleService;
import core.admin.location.service.ContentService;
import core.admin.location.service.ExpertService;
import core.admin.location.service.OfficeService;
import core.comm.BaseAction;
import core.comm.Propertie;
import entity.Article;
import entity.Expert;
import entity.Office;

/**
 * 所有一级导航页
 *
 * @author 王涛
 * 2016年3月24日
 */
@SuppressWarnings("serial")
@Controller()
@Scope("prototype")
public class WebNaviAction extends BaseAction{
	
	@Resource
	private ExpertService expertService;
	@Resource
	private OfficeService officeService;
	@Resource
	private ContentService contentService;
	@Resource
	private ArticleService articleService;
	
	/**
	 * 首页 
	 * @author 王涛
	 * 2016年3月24日
	 */
	public String index () {
		// 排序按权重排
		List<Expert> expertList = expertService.findList(new ArrayList<Propertie>(), 1, 4);
		results.put("expert", expertList);
		
		// 查询科室介绍
		List<Office> officeList = officeService.findList(new ArrayList<Propertie>(), 1, 4);
		results.put("office", officeList);
		
		// 最新动态
		List<Propertie> propsArticle = new ArrayList<Propertie>();
		// propsArticle.add(new Propertie("where", "", "=", ""));
		List<Article> articleList = articleService.findList(propsArticle, 1, 3);
		results.put("news", articleList);
		
		// 经典案例
		List<Propertie> propsArticle2 = new ArrayList<Propertie>();
		propsArticle2.add(new Propertie("where", "id", "=", 6));
		Article article = articleService.findByParam(propsArticle2);
		results.put("article", article);
		
		// 查询首页位置信息
		loadLocationContent(1);
		return SUCCESS;
	}
	
	/**
	 * 医院概览
	 * @author 王涛
	 * 2016年3月24日
	 */
	public String overview () {
		// 查询首页位置信息
		loadLocationContent(1);
		// 查询医院概览位置信息
		loadLocationContent(2);
		return SUCCESS;
	}
	
	/**
	 * 新闻动态
	 * @author 王涛
	 * 2016年3月24日
	 */
	public String news () {
		// 查询首页位置信息
		loadLocationContent(1);
		List<Propertie> props = new ArrayList<Propertie>();
		// TODO 查询条件
		// props.add(new Propertie("where", "", "=", ""));
		List<Article> articleList = articleService.findList(props, page, 10, "add_time desc");
		count = articleService.findCount(props);
		results.put("list", articleList);
		results.put("page", page);
		results.put("limit", 10);
		results.put("count", count);
		return SUCCESS;
	}
	
	/**
	 * 科室介绍 
	 * @author 王涛
	 * 2016年3月24日
	 */
	public String office () {
		// 查询首页位置信息
		loadLocationContent(1);
		List<Propertie> props = new ArrayList<Propertie>();
		// props.add(new Propertie("where", "", "=", ""));
		// 排序最好有权重
		List<Office> officeList = officeService.findList(props, page, 10, null);
		count = officeService.findCount(props);
		results.put("list", officeList);
		results.put("page", page);
		results.put("count", count);
		results.put("limit", 10);
		return SUCCESS;
	}
	
	/**
	 * 科室内页 
	 * @author 王涛
	 * 2016年4月11日
	 */
	public String officeContent () {
		// 查询首页位置信息
		loadLocationContent(1);
		Office office = officeService.findById(id);
		results.put("office", office);
		return SUCCESS;
	}
	
	/**
	 * 专家介绍
	 * @author 王涛
	 * 2016年3月24日
	 */
	public String expert () {
		// 查询首页位置信息
		loadLocationContent(1);
		List<Propertie> props = new ArrayList<Propertie>();
		// props.add(new Propertie("where", "", "=", ""));
		// 排序最好有权重
		List<Expert> expertList = expertService.findList(props, page, 10, null);
		count = expertService.findCount(props);
		results.put("list", expertList);
		results.put("page", page);
		results.put("count", count);
		results.put("limit", 10);
		return SUCCESS;
	} 
	
	/**
	 * 联系我们 
	 * @author 王涛
	 * 2016年3月24日
	 */
	public String contacts () {
		// 查询首页位置信息
		loadLocationContent(1);
		// 查询首页位置信息
		loadLocationContent(6);
		return SUCCESS;
	}
}
