package core.web.index;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import core.comm.BaseAction;
import util.FreeMarkerUtil;

@SuppressWarnings("serial")
@Controller("indexAction")
@Scope("prototype")
public class WebIndexAction extends BaseAction{
	
	public void freeze () {
		Map<String, Object> results = new HashMap<>();
		String templateName = "/index.ftl";
		String fileName = "/index.html";
		FreeMarkerUtil.analysisTemplate(templateName, fileName, results);
	}
	
	public Map<String, Object> getResults() {
		return results;
	}
	
}
