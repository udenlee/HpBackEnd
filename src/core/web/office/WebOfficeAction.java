package core.web.office;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import core.comm.BaseAction;

@SuppressWarnings("serial")
@Controller()
@Scope("prototype")
public class WebOfficeAction extends BaseAction{
	
}
