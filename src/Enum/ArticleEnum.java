package Enum;

public enum ArticleEnum {
	草稿(1), 发布(2), 删除(3);
	
	private int nCode;
	
	private ArticleEnum(int _nCode) {
		this.nCode = _nCode;
	}
	
	@Override
	public String toString() {
		return String.valueOf(this.nCode);
	}
}
